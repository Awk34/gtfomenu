﻿using System;
using Agents;
using CellMenu;
using ChainedPuzzles;
using Enemies;
using Gear;
using Harmony;
using MelonLoader;
using Player;
using UnhollowerRuntimeLib;
using UnityEngine;

namespace gtfomenu
{
	public static class BuildInfo
	{
		public const string Name = "gtfomenu";
		public const string Author = "GP";
		public const string Company = null;
		public const string Version = "1.0.0";
		public const string DownloadLink = "https://discord.gg/3Apx3JTr2Q";
	}
	public class gtfoloader : MelonMod
	{
		public override void OnApplicationStart()
		{
			Application.targetFrameRate = 200;
			ClassInjector.RegisterTypeInIl2Cpp<TrainerComponent>();
			MelonLogger.Log("TRAINER LOADED!");
			Current = this;
		}

		public void OnMainGameStart()
		{
			if (this.trainer != null)
			{
				return;
			}
			this.trainer = new GameObject().AddComponent<TrainerComponent>();
			UnityEngine.Object.DontDestroyOnLoad(this.trainer);
			TrainerComponent.Start();
			MelonLogger.Log("Loaded Trainer!");
		}
		public override void OnUpdate()
		{
			trainer.OnUpdate();
		}

		#region declarations
		public static gtfoloader Current;
		private TrainerComponent trainer;
        #endregion

    }
}
