﻿using System;
using Player;
using UnityEngine;

namespace gtfomenu
{
	public class CustomHudInfo : MonoBehaviour
	{

		public void GetHud()
		{
			if (this.m_custom_watermark == null)
			{
				this.m_custom_watermark = new WatermarkGuiLayer();
				this.m_custom_watermark.m_watermark = GuiManager.WatermarkLayer.AddRectComp("Gui/Player/PUI_Watermark", GuiAnchor.BottomRight, new Vector2(-30f, 50f), null).GetComponent<PUI_Watermark>();
				this.m_custom_watermark.m_watermark.SetFPSVisible(true);
			}
			this.m_custom_watermark.m_watermark.m_watermarkText.text = "Only Got past the watermark creations";
		}


		private void GetAmmoInfoFromSlot(out string infotext, PlayerAgent player)
		{
			string text = "<color={AmmoQuanityHash}>";
			string text2 = "";
			PlayerBackpack backpack = PlayerBackpackManager.GetBackpack(player.Owner);
			PlayerAmmoStorage ammoStorage = backpack.AmmoStorage;
			Color b = ColorExt.Hex("08FF00");
			Color a = ColorExt.Hex("FF0000");
			float num = 0f;
			float num2 = 0f;
			BackpackItem backpackItem;
			if (backpack.TryGetBackpackItem(InventorySlot.GearStandard, out backpackItem))
			{
				ItemEquippable itemEquippable = backpackItem.Instance.TryCast<ItemEquippable>();
				float num3 = ammoStorage.StandardAmmo.RelInPack / 0.8f;
				if (num3 > 1f)
				{
					num3 = 1f;
				}
				Color color = Color.Lerp(a, b, num3);
				if (itemEquippable != null)
				{
					num2 += ammoStorage.StandardAmmo.RelInPack;
					num += 1f;
					text2 = string.Concat(new string[]
					{
						text2,
						text.Replace("{AmmoQuanityHash}", "#" + ColorUtility.ToHtmlStringRGB(color)),
						itemEquippable.ArchetypeName,
						" ",
						(ammoStorage.StandardAmmo.RelInPack * 100f).ToString("N0"),
						"%</color>"
					});
				}
			}
			BackpackItem backpackItem2;
			if (backpack.TryGetBackpackItem(InventorySlot.GearSpecial, out backpackItem2))
			{
				ItemEquippable itemEquippable2 = backpackItem2.Instance.TryCast<ItemEquippable>();
				float num4 = ammoStorage.SpecialAmmo.RelInPack / 0.8f;
				if (num4 > 1f)
				{
					num4 = 1f;
				}
				Color color2 = Color.Lerp(a, b, num4);
				if (itemEquippable2 != null)
				{
					num += 1f;
					num2 += ammoStorage.SpecialAmmo.RelInPack;
					text2 = string.Concat(new string[]
					{
						text2,
						"\n",
						text.Replace("{AmmoQuanityHash}", "#" + ColorUtility.ToHtmlStringRGB(color2)),
						itemEquippable2.ArchetypeName,
						" ",
						(ammoStorage.SpecialAmmo.RelInPack * 100f).ToString("N0"),
						"%</color>"
					});
				}
			}
			BackpackItem backpackItem3;
			if (backpack.TryGetBackpackItem(InventorySlot.GearClass, out backpackItem3))
			{
				ItemEquippable itemEquippable3 = backpackItem3.Instance.TryCast<ItemEquippable>();
				float num5 = ammoStorage.ClassAmmo.RelInPack / 0.8f;
				if (num5 > 1f)
				{
					num5 = 1f;
				}
				bool flag = backpackItem3.Status == eInventoryItemStatus.Deployed;
				Color color3 = Color.Lerp(a, b, num5);
				if (itemEquippable3 != null)
				{
					text2 = string.Concat(new string[]
					{
						text2,
						"\n",
						text.Replace("{AmmoQuanityHash}", flag ? "red" : ("#" + ColorUtility.ToHtmlStringRGB(color3))),
						itemEquippable3.ArchetypeName,
						" ",
						flag ? "DEPLOYED" : ((ammoStorage.ClassAmmo.RelInPack * 100f).ToString("N0") + "%"),
						"</color>"
					});
				}
			}
			BackpackItem backpackItem4;
			if (backpack.TryGetBackpackItem(InventorySlot.ResourcePack, out backpackItem4))
			{
				ItemEquippable itemEquippable4 = backpackItem4.Instance.TryCast<ItemEquippable>();
				Color color4 = ColorExt.Hex("00FFF3");
				if (itemEquippable4 != null || ammoStorage.ResourcePackAmmo.RelInPack <= 0f)
				{
					text2 = string.Concat(new string[]
					{
						text2,
						"\n",
						"<color=#" + ColorUtility.ToHtmlStringRGB(color4) + ">",
						itemEquippable4.ArchetypeName + "</color>",
						" ",
						text.Replace("{AmmoQuanityHash}", "#" + ColorUtility.ToHtmlStringRGB(color4)),
						(ammoStorage.ResourcePackAmmo.RelInPack * 100f).ToString("N0"),
						"%</color>"
					});
				}
				else
				{
					text2 = string.Concat(new string[]
					{
						text2,
						"\n",
						"<color=#009B93><EMPTY RESOURCE SLOT></color>"
					});
				}
			}
			else
			{
				text2 = string.Concat(new string[]
				{
					text2,
					"\n",
					"<color=#009B93><EMPTY RESOURCE SLOT></color>"
				});
			}
			BackpackItem backpackItem5;
			if (backpack.TryGetBackpackItem(InventorySlot.Consumable, out backpackItem5))
			{
				ItemEquippable itemEquippable5 = backpackItem5.Instance.TryCast<ItemEquippable>();
				Color color5 = ColorExt.Hex("00FFF3");
				if (backpackItem5 != null || ammoStorage.ConsumableAmmo.RelInPack <= 0f)
				{
					text2 = string.Concat(new string[]
					{
						text2,
						"\n",
						"<color=#" + ColorUtility.ToHtmlStringRGB(color5) + ">",
						itemEquippable5.ArchetypeName + "</color>",
						" ",
						text.Replace("{AmmoQuanityHash}", "#" + ColorUtility.ToHtmlStringRGB(color5)),
						(ammoStorage.ConsumableAmmo.RelInPack * 100f).ToString("N0"),
						"%</color>"
					});
				}
				else
				{
					text2 = string.Concat(new string[]
					{
						text2,
						"\n",
						"<color=#009B93><EMPTY CONSUMABLE SLOT></color>"
					});
				}
			}
			else
			{
				text2 = string.Concat(new string[]
				{
					text2,
					"\n",
					"<color=#009B93><EMPTY CONSUMABLE SLOT></color>"
				});
			}
			float num6 = num2 / num / 0.8f;
			if (num6 > 1f)
			{
				num6 = 1f;
			}
			Color color6 = Color.Lerp(a, b, num6);
			text2 = string.Concat(new string[]
			{
				"<color=#",
				ColorUtility.ToHtmlStringRGB(color6),
				">",
				"Overall Ammo ",
				(num2 / num * 100f).ToString("N0"),
				"%</color>\n",
				text2
			});
			text = text2;
			infotext = text;
		}
		private void UpdateAmmoInformationPlayer(PlayerAgent playerAgent, out string text)
		{
			int playerSlotIndex = playerAgent.PlayerSlotIndex;
			text = "<color=#{healthcolorhash}>{health}%</color> <color=#00FEEF>{infection}</color> {IsDowned} <color=#{hashcolor}>{playername}</color>\n{AmmoInformation}<color=white>In Zone: <color=yellow>{#}</color>{suffix}</color>";
			float healthRel = playerAgent.Damage.GetHealthRel();
			float num = healthRel * 100f;
			float num2 = num / 100f;
			float num3 = (healthRel - 0.2f) / 0.8f;
			Color color = Color.Lerp(ColorExt.Hex("FF0000"), Color.green, (num3 < 0f) ? 0f : num3);
			text = text.Replace("{healthcolorhash}", ColorUtility.ToHtmlStringRGB(color));
			text = text.Replace("{health}", num.ToString("N0"));
			if (playerAgent.Damage.Infection < 0.1f)
			{
				text = text.Replace("{infection}", "");
			}
			else
			{
				text = text.Replace("{infection}", (playerAgent.Damage.Infection * 100f).ToString("N0") + "%");
			}
			text = text.Replace("{playername}", playerAgent.PlayerName);
			text = text.Replace("{IsDowned}", (!playerAgent.Alive) ? "<color=red>DOWNED</color>" : "");
			text = text.Replace("{#}", playerAgent.GetCourseNode().m_area.m_zone.Alias.ToString());
			text = text.Replace("{suffix}", playerAgent.GetCourseNode().m_area.m_navInfo.Suffix);
			text = text.Replace("{hashcolor}", ColorUtility.ToHtmlStringRGB(playerAgent.Owner.PlayerColor));
			string text2;
			this.GetAmmoInfoFromSlot(out text2, playerAgent);
			if (text2 != null)
			{
				text = text.Replace("{AmmoInformation}", text2 + "\n");
				return;
			}
			text = text.Replace("{AmmoInformation}", "");
		}
		public void UpdateWaterMarkText()
		{
			if (this.player != null && this.player.GetCourseNode() != null && this.player.GetCourseNode().m_area != null)
			{
				this.UpdateAmmoInformationPlayer(this.player, out TeamInformation);
			}
		}
		public bool DoesPlayerAgentExist()
		{
			return this.player != null;
		}
		public CustomHudInfo(int PlayerID, PlayerAgent player = null)
		{
			this.ID = PlayerID;
			this.player = player;
		}

		private WatermarkGuiLayer m_custom_watermark;
		private bool AllowLocalInfo = true;
		public string TeamInformation;
		public int ID;
		public PlayerAgent player;
		private CustomHudInfo[] teaminfo;
	}
}
