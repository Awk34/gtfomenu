﻿using MelonLoader;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle(gtfomenu.BuildInfo.Name)]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany(gtfomenu.BuildInfo.Company)]
[assembly: AssemblyProduct(gtfomenu.BuildInfo.Name)]
[assembly: AssemblyCopyright("Created by " + gtfomenu.BuildInfo.Author)]
[assembly: AssemblyTrademark(gtfomenu.BuildInfo.Company)]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
//[assembly: Guid("")]
[assembly: AssemblyVersion(gtfomenu.BuildInfo.Version)]
[assembly: AssemblyFileVersion(gtfomenu.BuildInfo.Version)]
[assembly: NeutralResourcesLanguage("en")]
[assembly: MelonInfo(typeof(gtfomenu.gtfoloader), gtfomenu.BuildInfo.Name, gtfomenu.BuildInfo.Version, gtfomenu.BuildInfo.Author, gtfomenu.BuildInfo.DownloadLink)]


// Create and Setup a MelonModGame to mark a Mod as Universal or Compatible with specific Games.
// If no MelonModGameAttribute is found or any of the Values for any MelonModGame on the Mod is null or empty it will be assumed the Mod is Universal.
// Values for MelonModGame can be found in the Game's app.info file or printed at the top of every log directly beneath the Unity version.
[assembly: MelonGame(null, null)]