﻿using MelonLoader;
using System;
using Agents;
using ChainedPuzzles;
using Enemies;
using Gear;
using Harmony;
using Player;
using UnhollowerRuntimeLib;
using FX_EffectSystem;
using AIGraph;
using LevelGeneration;
using GameData;
using System.IO;
using CellMenu;
using SNetwork;
using Il2CppSystem.Collections.Generic;
using UnhollowerBaseLib;using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Steamworks;
using GuiMenu;
using System.Linq;
using Globals;
using DropServer;
using AK;
using TMPro;

namespace gtfomenu
{
	public class TrainerComponent : MonoBehaviour
	{
		public TrainerComponent(IntPtr ptr) : base(ptr)
		{
			teaminfo = new CustomHudInfo[4];
			trainercom = this;
		}

        public class RundownDownData : MonoBehaviour
        {
            public RundownDownData(IntPtr ptr) : base(ptr)
            {
            }
            public eRundownTier tier;
            public int index;
            public uint RundownID;
        }

        public static RundownDownData CreateRundownSelect(eRundownTier tier, int index, uint RundownID)
		{
			RundownDownData rundownDownData = new GameObject().AddComponent<RundownDownData>();
			rundownDownData.index = index;
			rundownDownData.tier = tier;
			rundownDownData.RundownID = RundownID;
			return rundownDownData;
		}


		[Serializable]
		public class Block
		{
			public int Type { get; set; }
			public string GearJSON { get; set; }
			public string name { get; set; }
			public bool internalEnabled { get; set; }
			public int persistentID { get; set; }
		}
		[Serializable]
		public class Root
		{
			public List<object> Headers { get; set; }
			public List<Block> Blocks { get; set; }
			public int LastPersistentID { get; set; }
		}


		public static Rect DropDownRect;
		public static Vector2 ListScrollPos;
		public static bool DropdownVisible;
		public static int SelectedListItem;
		public class GuiListItem
		{
			public bool Selected;
			public string Name;
			public GuiListItem(bool mSelected, string mName)
			{
				Selected = mSelected;
				Name = mName;
			}
			public GuiListItem(string mName)
			{
				Selected = false;
				Name = mName;
			}
			public void enable()
			{
				Selected = true;
			}
			public void disable()
			{
				Selected = false;
			}
		}

		public class RundownData
		{
			public static int ExpIndex;
			public RundownData(eRundownTier tier, int index, uint ID, string name)
			{
				Tier = tier;
				RundownID = ID;
				ExpIndex = index;
				Name = name;
			}
		}

		public class RundownList 
		{
			public RundownList()
			{
				Il2CppSystem.Collections.Generic.List<RundownData> list = new Il2CppSystem.Collections.Generic.List<RundownData>();
				Dictionary<string, uint> dictionary = new Dictionary<string, uint>();
				for (uint num = 1U; num < 0x3E8U; num += 1U)
				{
					dictionary.Add("Rundown " + num.ToString(), num);
				}
				foreach (KeyValuePair<string, uint> keyValuePair in dictionary)
				{
					AddExpiditonsOnRundownList(keyValuePair.Value);
				}
			}

		}


		public static void healmedamnit()
		{
				PlayerAgent localPlayerAgent = PlayerManager.GetLocalPlayerAgent();
				PlayerBackpackManager.PickupHealthRel(localPlayerAgent.Damage.HealthMax - localPlayerAgent.Damage.Health, localPlayerAgent);
				GuiManager.PlayerLayer.m_playerStatus.m_healthText.SetText("#GOD#");
		}

		public static void healteam()
		{
			Il2CppSystem.Collections.Generic.List<PlayerAgent> playersingame = PlayerManager.PlayerAgentsInLevel;
			foreach (PlayerAgent pgn in playersingame)
			{
				if (!pgn.IsLocallyOwned)
                {
					pgn.Damage.Health = pgn.Damage.HealthMax;

				}

			}
		}
		public static void AddExpiditonsOnRundownList(uint customRundownID)
		{
			RundownDataBlock block = GameDataBlockBase<RundownDataBlock>.GetBlock(customRundownID);
			if (block == null)
			{
				return;
			}
			if (block.TierA != null && block.TierA.Count > 0)
			{
				for (int i = 0; i < block.TierA.Count; i++)
				{
					TrainerComponent.list.Add(TrainerComponent.CreateRundownSelect(eRundownTier.TierA, i, customRundownID));
				}
			}
			if (block.TierB != null && block.TierB.Count > 0)
			{
				for (int j = 0; j < block.TierB.Count; j++)
				{
					TrainerComponent.list.Add(TrainerComponent.CreateRundownSelect(eRundownTier.TierB, j, customRundownID));
				}
			}
			if (block.TierC != null && block.TierC.Count > 0)
			{
				for (int k = 0; k < block.TierC.Count; k++)
				{
					TrainerComponent.list.Add(TrainerComponent.CreateRundownSelect(eRundownTier.TierC, k, customRundownID));
				}
			}
			if (block.TierD != null && block.TierD.Count > 0)
			{
				for (int l = 0; l < block.TierD.Count; l++)
				{
					TrainerComponent.list.Add(TrainerComponent.CreateRundownSelect(eRundownTier.TierD, l, customRundownID));
				}
			}
			if (block.TierE != null && block.TierE.Count > 0)
			{
				for (int m = 0; m < block.TierE.Count; m++)
				{
					TrainerComponent.list.Add(TrainerComponent.CreateRundownSelect(eRundownTier.TierE, m, customRundownID));
				}
			}
		}
	
		public static void fastrun()
		{
			PlayerAgent localPlayerAgent = PlayerManager.GetLocalPlayerAgent();
			if (origspeed == -1)
            {
				origspeed = localPlayerAgent.PlayerData.runMoveSpeed;
			}
			if (brun)
				localPlayerAgent.PlayerData.runMoveSpeed = 15; ;
			if (!brun)
				localPlayerAgent.PlayerData.runMoveSpeed = origspeed;
		}



		public static void Highjump(float height)
		{
			PlayerAgent localPlayerAgent = PlayerManager.GetLocalPlayerAgent();
			if (orig1 == -1)
			{
				orig1 = localPlayerAgent.PlayerData.jumpVelInitial;
				orig2 = localPlayerAgent.PlayerData.jumpVerticalVelocityMax;
				orig3 = localPlayerAgent.PlayerData.airMoveSpeed;
			}
			if (bjump)
			{
				localPlayerAgent.PlayerData.jumpVelInitial = height;
				localPlayerAgent.PlayerData.jumpVerticalVelocityMax = height;
				//localPlayerAgent.PlayerData.airMoveSpeed = height;
			}
			if (!bjump)
			{
				localPlayerAgent.PlayerData.jumpVelInitial = orig1;
				localPlayerAgent.PlayerData.jumpVerticalVelocityMax = orig2;
				//localPlayerAgent.PlayerData.airMoveSpeed = orig3;
			}
		}

		public static void foamall()
		{
			GlueVolumeDesc vol;
			vol.currentScale = 10000f;
			vol.expandVolume = 10000f;
			vol.volume = 10000f;
			Il2CppSystem.Collections.Generic.List<EnemyAgent> nearbyenemies = AIG_CourseGraph.GetReachableEnemiesInNodes(PlayerManager.GetLocalPlayerAgent().CourseNode, 1000);
			for (int i = 0; i < nearbyenemies.Count; i++)
			{
				EnemyAgent enemyAgent = nearbyenemies[i];
				ProjectileManager.WantToSpawnGlueOnEnemyAgent(0, nearbyenemies[i], 1, nearbyenemies[i].transform.position, vol);
				ProjectileManager.WantToSpawnGlueOnEnemyAgent(0, enemyAgent, 1, enemyAgent.EyePosition, vol);
				ProjectileManager.WantToSpawnGlueOnEnemyAgent(0, enemyAgent, 1, enemyAgent.transform.position, vol);
			}

		}

		public static void troll()
		{
			Il2CppSystem.Collections.Generic.List<PlayerAgent> playersingame = PlayerManager.PlayerAgentsInLevel;
			foreach (PlayerAgent pgn in playersingame)
			{
				for (int i = 0; i < playersingame.Count; i++)
				{
					//if (pgn != PlayerManager.GetLocalPlayerAgent() && pgn != null)
					//{
					PlayerAgent pag = playersingame[i];
					//pItemData data = default(pItemData);
					//ItemSpawnManager.SpawnItem(138U, ItemMode.Pickup, pgn.Position, PlayerManager.GetLocalPlayerAgent().Rotation, true, data, null);
					//ItemSpawnManager.SpawnItem(148U, ItemMode.Pickup, pgn.Position, PlayerManager.GetLocalPlayerAgent().Rotation, true, data, null);
					//ItemSpawnManager.SpawnItem(151U, ItemMode.Pickup, pgn.Position, PlayerManager.GetLocalPlayerAgent().Rotation, true, data, null);
					//ItemSpawnManager.SpawnItem(151U, ItemMode.Instance, pgn.Position, PlayerManager.GetLocalPlayerAgent().Rotation, true, data, null);
					//ItemSpawnManager.SpawnItem(138U, ItemMode.Instance, pgn.Position, PlayerManager.GetLocalPlayerAgent().Rotation, true, data, null);
					//ItemSpawnManager.SpawnItem(148U, ItemMode.Instance, pgn.Position, PlayerManager.GetLocalPlayerAgent().Rotation, true, data, null);
					pgn.Damage.FireDamage(100f, pgn);
					pgn.Damage.FallDamage(1000f);
					pgn.PlayerData.runMoveSpeed = 100f;
					pag.PlayerData.runMoveSpeed = 100f;
					pag.PlayerData.jumpGravityMulDefault = 50f;
					//PlayerChatManager.WantToSentTextMessage(pag, "I am : " + pag.PlayerName);

					//Agents.AgentReplicatedActions.PlayerReviveAction(pgn, pgn, Vector3.zero);
					//}
				}
			}
		}


		public static void openalldoors()
		{
			iLG_Door_Core[] componentsInChildren = Builder.Current.m_currentFloor.GetComponentsInChildren<iLG_Door_Core>(true);
			for (int i = 0; i < componentsInChildren.Length; i++)
			{
				if (componentsInChildren[i] != null)
				{
					componentsInChildren[i].AttemptDamage(eDoorDamageType.EnemyHeavy, new Vector3(0, 0, 0));
					componentsInChildren[i].AttemptOpenCloseInteraction();
					enemysleep();
				}
				enemysleep();
			}
			enemysleep();
		}

		public static void enemysleep()
		{
			foreach (EnemyAgent eai in themembers)
			{
				eai.AI.m_behaviour.ChangeState(EB_States.Hibernating);
			}
		}


		public static iHackable hackable;
		
		public static void hackall()
		{
			iHackable[] hackables = Builder.Current.m_currentFloor.GetComponentsInChildren<iHackable>(true);
			foreach (iHackable ji in hackables)
            {
				LG_LevelInteractionManager.WantToSetHackableStatus(ji, eHackableStatus.Success, PlayerManager.GetLocalPlayerAgent());
			}
		}

		public static void removelocks()
		{
			iLG_Lock[] locks = Builder.Current.m_currentFloor.GetComponentsInChildren<iLG_Lock>(true);
			foreach (iLG_Lock lok in locks)
			{
				LG_LevelInteractionManager.WantToUnlockLock(lok.LockID);
			}
		}




		public static void makeemmove()
		{
			EnemyGroup egrp = new EnemyGroup();
			PlayerManager mkr = new PlayerManager();
			int inlvl = egrp.Members.Count;
			int pinlvl = PlayerManager.PlayerAgentsInLevel.Count;
			MelonLogger.Log("Enemy inlvl: " + inlvl);
			MelonLogger.Log("Player inlvl: " + pinlvl);
			foreach (EnemyAgent ggg in egrp.Members)
			{
				MelonLogger.Log("inlvl " + ggg.name);
			}
			EnemyAI dsdds = new EnemyAI();
			for (int i = 0; i < inlvl; i++)
			{
				EnemyAgent bg = dsdds.m_group.Members[i];
				if (bg != null)
				{
					PlayerChatManager.WantToSentTextMessage(PlayerManager.GetLocalPlayerAgent(), "enem: " + bg.name);
				}
			}
		}

		public void soundoff()
		{
			foreach (PlayerAgent pg in PlayerManager.PlayerAgentsInLevel)
			{
				PlayerChatManager.WantToSentTextMessage(pg, "My Name is: " + "[" + pg.PlayerName + "]" + " I have: " + "[" + (pg.Damage.Health / pg.Damage.HealthMax) * 100 + "%" + "]" + " Health");
				PlayerChatManager.WantToSentTextMessage(pg, "Im currntly holding :" + "[" + pg.Inventory.WieldedItem.ToString() + "]");
				pg.FPSCamera.m_punchDirTest = new Vector3(40f, 40f, 40f);

			}
		}


		static void reviveall()
		{
			Agents.AgentReplicatedActions.PlayerReviveAction(PlayerManager.GetLocalPlayerAgent(), PlayerManager.GetLocalPlayerAgent(), Vector3.zero);
			Agents.AgentReplicatedActions.PlayerReviveAction(null, PlayerManager.GetLocalPlayerAgent(), Vector3.zero);

		}

		public static void enemydraw()
		{
			PlayerAgent localPlayerAgent = PlayerManager.GetLocalPlayerAgent();
			PlayerInventoryLocal playerInventoryLocal = localPlayerAgent.Inventory.TryCast<PlayerInventoryLocal>();
			Il2CppSystem.Collections.Generic.List<EnemyAgent> reachableEnemiesInNodes = AIG_CourseGraph.GetReachableEnemiesInNodes(localPlayerAgent.CourseNode, 0x32);
			var cam = CameraManager.GetCurrentCamera();

			#region Bone Vectors
			if (reachableEnemiesInNodes != null && reachableEnemiesInNodes.Count > 0)
			{
				for (int i = 0; i < reachableEnemiesInNodes.Count; i++)
				{
					EnemyAgent enemyag = reachableEnemiesInNodes[i];
					if (enemyag != null && enemyag.Alive)
					{
						var enemyrightarm = new Vector3(
							cam.WorldToScreenPoint(enemyag.ModelRef.m_rightArmBone.position).x,
							cam.WorldToScreenPoint(enemyag.ModelRef.m_rightArmBone.position).y,
							cam.WorldToScreenPoint(enemyag.ModelRef.m_rightArmBone.position).z);
						var enemyleftarm = new Vector3(
							cam.WorldToScreenPoint(enemyag.ModelRef.m_leftArmBone.position).x,
							cam.WorldToScreenPoint(enemyag.ModelRef.m_leftArmBone.position).y,
							cam.WorldToScreenPoint(enemyag.ModelRef.m_leftArmBone.position).z);
						var enemyrighthand = new Vector3(
							cam.WorldToScreenPoint(enemyag.ModelRef.m_rightHandBone.position).x,
							cam.WorldToScreenPoint(enemyag.ModelRef.m_rightHandBone.position).y,
							cam.WorldToScreenPoint(enemyag.ModelRef.m_rightHandBone.position).z);
						var enemylefthand = new Vector3(
							cam.WorldToScreenPoint(enemyag.ModelRef.m_leftHandBone.position).x,
							cam.WorldToScreenPoint(enemyag.ModelRef.m_leftHandBone.position).y,
							cam.WorldToScreenPoint(enemyag.ModelRef.m_leftHandBone.position).z);
						var enemyneck = new Vector3(
							cam.WorldToScreenPoint(enemyag.ModelRef.m_neckBone.position).x,
							cam.WorldToScreenPoint(enemyag.ModelRef.m_neckBone.position).y,
							cam.WorldToScreenPoint(enemyag.ModelRef.m_neckBone.position).z);
						var enemyleftleg = new Vector3(
							cam.WorldToScreenPoint(enemyag.ModelRef.m_leftLegBone.position).x,
							cam.WorldToScreenPoint(enemyag.ModelRef.m_leftLegBone.position).y,
							cam.WorldToScreenPoint(enemyag.ModelRef.m_leftLegBone.position).z);
						var enemyright = new Vector3(
							cam.WorldToScreenPoint(enemyag.ModelRef.m_rightLegBone.position).x,
							cam.WorldToScreenPoint(enemyag.ModelRef.m_rightLegBone.position).y,
							cam.WorldToScreenPoint(enemyag.ModelRef.m_rightLegBone.position).z);
						var enemyhead = new Vector3(
							cam.WorldToScreenPoint(enemyag.ModelRef.m_headBone.position).x,
							cam.WorldToScreenPoint(enemyag.ModelRef.m_headBone.position).y,
							cam.WorldToScreenPoint(enemyag.ModelRef.m_headBone.position).z);
						var enemycenter = new Vector3(
							cam.WorldToScreenPoint(enemyag.ModelRef.m_chestBone.position).x,
							cam.WorldToScreenPoint(enemyag.ModelRef.m_chestBone.position).y,
							cam.WorldToScreenPoint(enemyag.ModelRef.m_chestBone.position).z);
						var enemyboundingvector = new Vector3(
							cam.WorldToScreenPoint(enemyag.transform.position).x,
							cam.WorldToScreenPoint(enemyag.transform.position).y,
							cam.WorldToScreenPoint(enemyag.transform.position).z);

						#endregion Bone Vectors


						float distance2 = Vector3.Distance(PlayerManager.GetLocalPlayerAgent().transform.position, enemyag.transform.position);
						float distance = (float)Math.Round(Vector3.Distance(cam.transform.position, enemyag.transform.position), 3);
						int sizeOfFont = 15;
						if (distance <= 50)
						{
							sizeOfFont = 15;
						}
						else if (distance < 100 && distance > 50)
						{ sizeOfFont = 13; }
						else if (distance > 100 && distance <= 200)
						{ sizeOfFont = 11; }
						else if (distance > 200 && distance <= 300)
						{ sizeOfFont = 9; }
						else
						{ sizeOfFont = 9; }

						var guistyle = new GUIStyle
						{ fontSize = sizeOfFont };

						Line(new Vector2(enemyneck.x, (float)Screen.height - enemyneck.y), new Vector2(enemyneck.x, (float)Screen.height - enemycenter.y), Color.green, 5);
						Line(new Vector2(enemyneck.x, (float)Screen.height - enemyneck.y), new Vector2(enemyhead.x, (float)Screen.height - enemyhead.y), Color.green, 5);
						Line(new Vector2(enemyrightarm.x, (float)Screen.height - enemyrightarm.y), new Vector2(enemyrighthand.x, (float)Screen.height - enemyrighthand.y), Color.green, 5);
						Line(new Vector2(enemyleftarm.x, (float)Screen.height - enemyleftarm.y), new Vector2(enemylefthand.x, (float)Screen.height - enemylefthand.y), Color.green, 5);
						Line(new Vector2(enemyleftleg.x, (float)Screen.height - enemyleftleg.y), new Vector2(enemycenter.x, (float)Screen.height - enemycenter.y), Color.green, 5);
						Line(new Vector2(enemyright.x, (float)Screen.height - enemyright.y), new Vector2(enemycenter.x, (float)Screen.height - enemycenter.y), Color.green, 5);
						GUI.Label(new Rect(new Vector2(enemyhead.x, enemyhead.y), new Vector2(100f, 100f)), "Distance :" + distance.ToString());
					}
				}
			}
		}

		public static void Line(Vector2 lineStart, Vector2 lineEnd, Color color, int thickness)
		{
			if (_coloredLineTexture == null || _coloredLineColor != color)
			{
				_coloredLineColor = color;
				_coloredLineTexture = new Texture2D(1, 1);
				_coloredLineTexture.SetPixel(0, 0, _coloredLineColor);
				_coloredLineTexture.wrapMode = 0;
				_coloredLineTexture.Apply();
			}

			var vector = lineEnd - lineStart;
			float pivot = 57.29578f * Mathf.Atan(vector.y / vector.x);
			//if (vector.x < 0f)
			//{
			//	pivot += 180f;
			//}

			if (thickness < 1)
			{
				thickness = 1;
			}

			int yOffset = (int)Mathf.Ceil((float)(thickness / 2));

			GUIUtility.RotateAroundPivot(pivot, lineStart);
			GUI.DrawTexture(new Rect(lineStart.x, lineStart.y - (float)yOffset, vector.magnitude, (float)thickness), _coloredLineTexture);
			GUIUtility.RotateAroundPivot(-pivot, lineStart);
		}

		public static void EnemyDataBlocksCustomized()
		{
			if (TrainerComponent.SeeShadows)
			{
				for (int i = 0; i < GameDataBlockBase<EnemyDataBlock>.Wrapper.Blocks.Count; i++)
				{
					EnemyDataBlock enemyDataBlock = GameDataBlockBase<EnemyDataBlock>.Wrapper.Blocks[i];
					if (enemyDataBlock != null && enemyDataBlock.name.Contains("Shadow"))
					{
						foreach (ModelData modelData in enemyDataBlock.ModelDatas)
						{
							modelData.ModelFile = "Assets/AssetPrefabs/CharacterBuilder/Enemies/Striker/Striker_CB.prefab";
						}
					}
				}
			}
		}

		public static void RecoilDataBlocks()
		{
			for (int i = 0; i < GameDataBlockBase<RecoilDataBlock>.Wrapper.Blocks.Count; i++)
			{
				RecoilDataBlock recoilDataBlock = GameDataBlockBase<RecoilDataBlock>.Wrapper.Blocks[i];
				if (recoilDataBlock != null)
				{
					recoilDataBlock.hipFireCrosshairSizeMax = 10f;
					recoilDataBlock.hipFireCrosshairSizeDefault = 10f;
					recoilDataBlock.hipFireCrosshairSizeMax = 10f;
					recoilDataBlock.spring = 0f;
					recoilDataBlock.power.Min = 0f;
					recoilDataBlock.power.Max = 0f;
					recoilDataBlock.horizontalScale.Min = 0f;
					recoilDataBlock.horizontalScale.Max = 0f;
					recoilDataBlock.verticalScale.Min = 0f;
					recoilDataBlock.verticalScale.Max = 0f;
					recoilDataBlock.directionalSimilarity = 0f;
					recoilDataBlock.recoilPosShiftWeight = 0f;
					recoilDataBlock.recoilPosStiffness = 0f;
					recoilDataBlock.recoilPosDamping = 0f;
					recoilDataBlock.recoilPosImpulseWeight = 0f;
					recoilDataBlock.recoilCameraPosWeight = 0f;
					recoilDataBlock.recoilAimingWeight = 0f;
					recoilDataBlock.recoilRotImpulse = new Vector3(0f, 0f, 0f);
					recoilDataBlock.recoilRotStiffness = 0f;
					recoilDataBlock.recoilRotDamping = 0f;
					recoilDataBlock.recoilRotImpulseWeight = 0f;
					recoilDataBlock.recoilCameraRotWeight = 0f;
				}
			}
		}


		public static void ArchetypeDataBlocksCustomized()
		{

			for (int i = 0; i < GameDataBlockBase<ArchetypeDataBlock>.Wrapper.Blocks.Count; i++)
			{
				ArchetypeDataBlock archetypeDataBlock = GameDataBlockBase<ArchetypeDataBlock>.Wrapper.Blocks[i];
				if (archetypeDataBlock != null)
				{
					archetypeDataBlock.internalEnabled = true;
					archetypeDataBlock.DamageFalloff *= new Vector2(archetypeDataBlock.DamageFalloff.y * 5f, archetypeDataBlock.DamageFalloff.y * 5f);
					archetypeDataBlock.StaggerDamageMulti *= 20f;
					archetypeDataBlock.AimTransitionTime = 0f;
					archetypeDataBlock.StaggerDamageMulti = 10f;
					archetypeDataBlock.ShotgunConeSize = 0;
					archetypeDataBlock.HipFireSpread = 0f;
					archetypeDataBlock.AimSpread = 0f;
				}
			}
		}

		public static void exptest()
		{
			DistributionAmountData dd = new DistributionAmountData();
			Il2CppArrayBase<ExpeditionBalanceDataBlock> allBlocks = GameDataBlockBase<ExpeditionBalanceDataBlock>.GetAllBlocks();
			Il2CppSystem.Collections.Generic.List<ExpeditionBalanceDataBlock> list = new Il2CppSystem.Collections.Generic.List<ExpeditionBalanceDataBlock>();
			if (allBlocks != null)
			{
				foreach (ExpeditionBalanceDataBlock eblock in allBlocks)
				{
					if (eblock != null)
					{
						eblock.internalEnabled = true;
						eblock.LootPerZone = 100;
						eblock.HealthPerZone = 100;
						eblock.StaticEnemiesMaxSmallArea = 100;
						eblock.StaticEnemiesMaxPerZone = 200;
						eblock.EnemyPopulationPerZone = 100f;
					}

				}
			}
		}

		//public DistributionAmountData()
		//{
		//	this.Some = 5;
		//	this.Many = 0x14;
		//	this.Tons = 0x28;
		//}

		public static void GetRundownBlocksCustomized()
		{
			Il2CppArrayBase<RundownDataBlock> allBlocks = GameDataBlockBase<RundownDataBlock>.GetAllBlocks();
			if (allBlocks != null)
			{
				foreach (RundownDataBlock rundownDataBlock in allBlocks)
				{
					if (rundownDataBlock != null)
					{
						rundownDataBlock.internalEnabled = true;
						Il2CppSystem.Collections.Generic.List<RundownTierProgressionData> list = new Il2CppSystem.Collections.Generic.List<RundownTierProgressionData>();
						list.Add(rundownDataBlock.ReqToReachTierB);
						list.Add(rundownDataBlock.ReqToReachTierC);
						list.Add(rundownDataBlock.ReqToReachTierD);
						list.Add(rundownDataBlock.ReqToReachTierE);
						foreach (ExpeditionInTierData expeditionInTierData in rundownDataBlock.TierA)
						{
							if (expeditionInTierData.Expedition != null)
							{
								expeditionInTierData.Accessibility = eExpeditionAccessibility.AlwaysAllow;
							}
						}
						foreach (ExpeditionInTierData expeditionInTierData2 in rundownDataBlock.TierB)
						{
							if (expeditionInTierData2.Expedition != null)
							{
								expeditionInTierData2.Accessibility = eExpeditionAccessibility.AlwaysAllow;
							}
						}
						foreach (ExpeditionInTierData expeditionInTierData3 in rundownDataBlock.TierC)
						{
							if (expeditionInTierData3.Expedition != null)
							{
								expeditionInTierData3.Accessibility = eExpeditionAccessibility.AlwaysAllow;
							}
						}
						foreach (ExpeditionInTierData expeditionInTierData4 in rundownDataBlock.TierD)
						{
							if (expeditionInTierData4.Expedition != null)
							{
								expeditionInTierData4.Accessibility = eExpeditionAccessibility.AlwaysAllow;
							}
						}
						foreach (ExpeditionInTierData expeditionInTierData5 in rundownDataBlock.TierE)
						{
							if (expeditionInTierData5.Expedition != null)
							{
								expeditionInTierData5.Accessibility = eExpeditionAccessibility.AlwaysAllow;
							}
						}
						for (int i = 0; i < list.Count; i++)
						{
							RundownTierProgressionData rundownTierProgressionData = list[i];
							if (rundownTierProgressionData != null)
							{
								rundownTierProgressionData.MainSectors = 0;
								rundownTierProgressionData.SecondarySectors = 0;
								rundownTierProgressionData.ThirdSectors = 0;
								rundownTierProgressionData.AllClearedSectors = 0;
							}
						}
					}
				}
			}
		}

		public static void GetAllPlayerOfflineGearDataBlocksEnabled()
		{
			for (int i = 0; i < GameDataBlockBase<PlayerOfflineGearDataBlock>.Wrapper.Blocks.Count; i++)
			{
				PlayerOfflineGearDataBlock playerOfflineGearDataBlock = GameDataBlockBase<PlayerOfflineGearDataBlock>.Wrapper.Blocks[i];
				if (playerOfflineGearDataBlock != null)
				{
					playerOfflineGearDataBlock.internalEnabled = true;
				}
			}
		}

		public static void RectFilled(float x, float y, float width, float height, Texture2D text)
		{
			GUI.DrawTexture(new Rect(x, y, width, height), text);
		}
		public static void RectOutlined(float x, float y, float width, float height, Texture2D text, float thickness = 1f)
		{
			RectFilled(x, y, thickness, height, text);
			RectFilled(x + width - thickness, y, thickness, height, text);
			RectFilled(x + thickness, y, width - thickness * 2f, thickness, text);
			RectFilled(x + thickness, y + height - thickness, width - thickness * 2f, thickness, text);
		}

		public static void Box(float x, float y, float width, float height, Texture2D text, float thickness = 1f)
		{
			RectOutlined(x - width / 2f, y - height, width, height, text, thickness);
		}
		public static void spawnitems2()
		{
			foreach (uint uu in shitlist)
			{
				pItemData data = default(pItemData);
				ItemSpawnManager.SpawnItem(uu, ItemMode.Pickup, PlayerManager.GetLocalPlayerAgent().Position, PlayerManager.GetLocalPlayerAgent().Rotation, true, data, null);
			}
		}

		public static void spawnsurvival()
		{
			PlayerAgent localPlayerAgent = PlayerManager.GetLocalPlayerAgent();
			AIG_CourseNode courseNode = localPlayerAgent.CourseNode;
			ushort num;
			Mastermind.Current.TriggerSurvivalWave(courseNode, 3U, 5U, out num, SurvivalWaveSpawnType.InRelationToClosestAlivePlayer);
		}



		private void EquipGear(uint ID)
		{
			ItemDataBlock block = GameDataBlockBase<ItemDataBlock>.GetBlock(ID);
			Il2CppArrayBase<ItemDataBlock> itemblock = GameDataBlockBase<ItemDataBlock>.GetAllBlocks();

			PlayerBackpackManager.ForceSyncLocalInventory(null);
			PlayerAgent localPlayerAgent = PlayerManager.GetLocalPlayerAgent();
			if (localPlayerAgent == null)
			{
				return;
			}
			localPlayerAgent.Sync.WantsToWieldSlot(block.inventorySlot, false);
		}

		private void UpdateStats()
		{
			bool flag = false;
			if (teaminfo == null)
			{
				teaminfo = new CustomHudInfo[4];
			}
			for (int i = 0; i < PlayerManager.PlayerAgentsInLevel.Count; i++)
			{
				PlayerAgent playerAgent = PlayerManager.PlayerAgentsInLevel[i];
				if (playerAgent != null && !playerAgent.IsLocallyOwned)
				{
					if (teaminfo[i] == null)
					{
						teaminfo[i] = new CustomHudInfo(playerAgent.PlayerSlotIndex, playerAgent);
					}
					if (teaminfo[i].DoesPlayerAgentExist())
					{
						teaminfo[i].UpdateWaterMarkText();
					}
					else
					{
						teaminfo[i] = null;
						flag = true;
					}
				}
			}
			if (flag)
			{
				teaminfo = null;
			}
		}

		public void GetWardenObjectiveInfo()
		{
			if (WardenObjectiveManager.Current.m_wardenObjectiveItem != null)
			{
				foreach (LG_LayerType key in new System.Collections.Generic.List<LG_LayerType>
				{
					LG_LayerType.MainLayer,
					LG_LayerType.SecondaryLayer,
					LG_LayerType.ThirdLayer
				})
				{
					iWardenObjectiveItem iWardenObjectiveItem = WardenObjectiveManager.Current.m_wardenObjectiveItem[key];
					if (iWardenObjectiveItem != null)
					{
						LG_WardenObjective_Reactor lg_WardenObjective_Reactor = iWardenObjectiveItem.TryCast<LG_WardenObjective_Reactor>();
						if (lg_WardenObjective_Reactor != null && lg_WardenObjective_Reactor.CurrentStateOverrideCode != null)
						{
							CurrentStateOverrideCode = "<color=purple>" + lg_WardenObjective_Reactor.CurrentStateOverrideCode.ToUpper() + "</color>";
						}
						return;
					}
				}
			}
			CurrentStateOverrideCode = "";
		}

		public static void UpdateZone(PlayerAgent LocalPlayerAgent)
		{
			if (LocalPlayerAgent.CourseNode != null)
			{
				AIG_CourseNode courseNode = LocalPlayerAgent.CourseNode;
				if (courseNode.m_zone != null && courseNode.m_area != null)
				{
					TrainerComponent.MyZone = "[In ZONE: <color=yellow>" + courseNode.m_zone.Alias.ToString() + "</color>" + courseNode.m_area.m_navInfo.Suffix + "]";
				}
			}
		}

		public static bool EnemyIsInSight(EnemyAgent enemyAgent)
		{
			PlayerAgent localPlayerAgent = PlayerManager.GetLocalPlayerAgent();
			if (localPlayerAgent == null)
			{
				return false;
			}
			Il2CppSystem.Collections.Generic.List<Vector3> list = new Il2CppSystem.Collections.Generic.List<Vector3>();
			list.Add(localPlayerAgent.EyePosition);
			list.Add(localPlayerAgent.Position);
			Il2CppSystem.Collections.Generic.List<Vector3> list2 = new Il2CppSystem.Collections.Generic.List<Vector3>();
			list2.Add(enemyAgent.EyePosition);
			list2.Add(enemyAgent.Position);
			list2.Add(enemyAgent.ListenerPosition);
			foreach (Vector3 vector in list)
			{
				foreach (Vector3 a in list2)
				{
					Ray ray = default(Ray);
					ray.origin = vector;
					Vector3 direction = a - vector;
					float magnitude = direction.magnitude;
					direction.Normalize();
					ray.direction = direction;
					RaycastHit raycastHit;
					if (!Physics.Raycast(ray, out raycastHit, magnitude, LayerManager.MASK_WORLD))
					{
						return true;
					}
				}
			}
			return false;
		}

		public static void UpdateSmartSneaking(PlayerAgent playerAgent)
		{
			if (smartsneak)
			{
				if (playerAgent.PlayerData == null)
				{
					return;
				}
				if (playerAgent.CourseNode == null)
				{
					return;
				}
				Il2CppSystem.Collections.Generic.List<EnemyAgent> reachableEnemiesInNodes = AIG_CourseGraph.GetReachableEnemiesInNodes(playerAgent.CourseNode, 2);
				if (reachableEnemiesInNodes != null && reachableEnemiesInNodes.Count > 0)
				{
					for (int i = 0; i < reachableEnemiesInNodes.Count; i++)
					{
						EnemyAgent enemyAgent = reachableEnemiesInNodes[i];
						bool flag = enemyAgent.IsHibernationDetecting && Clock.Time - enemyAgent.HibernationDetectingStartedAt > 0.8f;
						if (enemyAgent != null && enemyAgent.Alive && (playerAgent.Position - enemyAgent.ListenerPosition).sqrMagnitude < 80f && enemyAgent.AI.Mode == AgentMode.Hibernate && flag && TrainerComponent.EnemyIsInSight(enemyAgent))
						{
							playerAgent.PlayerData.crouchMoveSpeed = 0f;
							return;

						}
					}
				}
				playerAgent.PlayerData.crouchMoveSpeed = 2f;
			}
		}

		public static EnemyAgent[] AllEnemies()
		{
			return (EnemyAgent[])(FindObjectsOfType(Il2CppSystem.Type.GetTypeFromHandle(Il2CppSystem.Type.GetTypeHandle(new EnemyAgent()))));
		}

		public static void telebadguys()
		{
			PlayerAgent localPlayerAgent = PlayerManager.GetLocalPlayerAgent();
			Il2CppSystem.Collections.Generic.List<EnemyAgent> reachableEnemiesInNodes = AIG_CourseGraph.GetReachableEnemiesInNodes(localPlayerAgent.CourseNode, 20);
			RaycastHit raycastHit;
			if (Physics.Raycast(Camera.main.ScreenPointToRay(new Vector3((float)(Screen.width / 2), (float)(Screen.height / 2), 0f)), out raycastHit, 1000f))
			{
				Vector3 position = raycastHit.point;
				if (localPlayerAgent != null && localPlayerAgent.FPSCamera.CameraRayObject != null)
				{
					position = localPlayerAgent.FPSCamera.CameraRayObject.transform.position;

					foreach (EnemyAgent ddd in reachableEnemiesInNodes)
					{
						ddd.transform.position = position;
						ddd.gameObject.transform.position = position;
						ddd.Position = position;
						ddd.GoodPosition = position;
					}
				}
			}
		}

		public static void teleme()
		{
			RaycastHit raycastHit;
			if (Physics.Raycast(Camera.main.ScreenPointToRay(new Vector3((float)(Screen.width / 2), (float)(Screen.height / 2), 0f)), out raycastHit, 1000f))
			{
				Vector3 position = raycastHit.point;
				if (PlayerManager.GetLocalPlayerAgent() != null && PlayerManager.GetLocalPlayerAgent().FPSCamera.CameraRayObject != null)
				{
					PlayerManager.GetLocalPlayerAgent().transform.SetPositionAndRotation(PlayerManager.GetLocalPlayerAgent().FPSCamera.CameraRayObject.transform.position, PlayerManager.GetLocalPlayerAgent().Rotation);
					PlayerManager.GetLocalPlayerAgent().transform.position = PlayerManager.GetLocalPlayerAgent().FPSCamera.CameraRayObject.transform.position;
					PlayerManager.GetLocalPlayerAgent().gameObject.transform.position = PlayerManager.GetLocalPlayerAgent().FPSCamera.CameraRayObject.transform.position;
				}
			}
		}



		public static void testshit()
		{
			SNet_Replicator subManagerReplicator = SNet.SubManagerReplicator;
			Il2CppSystem.Collections.Generic.List<PlayerAgent> playersingame = PlayerManager.PlayerAgentsInLevel;
			PlayerAgent localPlayerAgent = PlayerManager.GetLocalPlayerAgent();
			PlayerBotManager.pPlayerBotCommand data = default(PlayerBotManager.pPlayerBotCommand);
			data.command = ePlayerBotCommand.Activate;
			data.commandLookPos = localPlayerAgent.EyePosition;
			data.sourcePlayer.SetPlayer(SNet.LocalPlayer);
			foreach (SNet_Player pag in SNet.SessionHub.PlayersInSession)
			{	if (!pag.IsLocal)
				{
					PlayerBotManager.Current.m_givePlayerBotCommand.Send(data, SNet_ChannelType.BotCommands, SNet_SendQuality.Reliable_WithBuffering, pag);
				}
			}
		}

		public static void pickupallitems()
		{
			iPickupItemSync[] componentsInChildren = Builder.Current.m_currentFloor.GetComponentsInChildren<iPickupItemSync>(true);
			PlayerAgent localPlayerAgent = PlayerManager.GetLocalPlayerAgent();
			for (int i = 0; i < componentsInChildren.Length; i++)
			{
				if (componentsInChildren[i] != null)
				{
					componentsInChildren[i].AttemptPickupInteraction(ePickupItemInteractionType.Pickup, localPlayerAgent.Owner);
				}
			}
		}


		public static void killincrosshairs()
		{
			PlayerAgent localPlayerAgent = PlayerManager.GetLocalPlayerAgent();
			if (localPlayerAgent != null && localPlayerAgent.FPSCamera.CameraRayObject != null)
			{
				EnemyAI enemyAI = localPlayerAgent.FPSCamera.CameraRayObject.GetComponentInParent<EnemyAI>();
				if (enemyAI != null)
				{
					enemyAI.m_enemyAgent.Damage.Health = 0f;
					enemyAI.m_enemyAgent.Damage.InstantDead(true);
				}
			}
		}


		public static void downplayer()
		{
			PlayerAgent localPlayerAgent = PlayerManager.GetLocalPlayerAgent();
			if (localPlayerAgent != null && localPlayerAgent.FPSCamera.CameraRayObject != null)
			{
				PlayerAgent pagent = localPlayerAgent.FPSCamera.CameraRayObject.GetComponentInParent<PlayerAgent>();
				if (pagent != null)
				{
					if (pagent != localPlayerAgent)
					{ 
						pagent.GiveHealth(-100f);
						pagent.Alive = false;
						pagent.Damage.FallDamage(1000f);
						pagent.Damage.Health = 0;
						pagent.Damage.GrabbedByTank = true;
						pagent.Damage.GlueDamage(10000f);
						pagent.Damage.MeleeDamage(1000f, pagent, pagent.Position, pagent.EyePosition);
						pagent.Damage.BulletDamage(10000f, pagent, pagent.Position, pagent.EyePosition, new Vector3(0, 0, 0), true, 100f, 1000f);
						pagent.Damage.Infection = 1000f;
						pagent.Damage.SendSetDead(true);
						
					}
				}
                else
                {
					MelonLogger.Log("No player in crosshair");
                }
			}
		}


		public static void restatlevel()
		{
			SNet.Capture.RecallGameState(eBufferType.RestartLevel);
		}
		public static void rundowntest()
        {


		}

		static void Completelevel()
		{
			
			if (RundownManager.ActiveExpedition != null)
			{
				pActiveExpedition activeExpeditionData = RundownManager.GetActiveExpeditionData();
				string data = activeExpeditionData.rundownKey.data;
				string uniqueExpeditionKey = RundownManager.GetUniqueExpeditionKey(data, activeExpeditionData.tier, activeExpeditionData.expeditionIndex);
				RundownManager.Current.SetLayerProgression(ExpeditionLayers.Main, LayerProgressionState.Completed);
				RundownManager.Current.SetLayerProgression(ExpeditionLayers.Secondary, LayerProgressionState.Completed);
				RundownManager.Current.SetLayerProgression(ExpeditionLayers.Third, LayerProgressionState.Completed);
				RundownManager.Current.ExpeditionSuccess();
				GameStateManager.ChangeState(eGameStateName.ExpeditionSuccess);
			}
		}

		public static void freecam()
        {
			PlayerAgent localPlayerAgent = PlayerManager.GetLocalPlayerAgent();
			if (FocusStateManager.Current.m_currentState == eFocusState.FPS)
			{
				FocusStateManager.Current.m_previousFocusState = FocusStateManager.Current.m_currentState;
				FocusStateManager.ChangeState(eFocusState.Freeflight, false);
				localPlayerAgent.Locomotion.m_owner.FPItemHolder.FPSArms.SetVisible(true);
				localPlayerAgent.Locomotion.Downed.m_owner.FPItemHolder.FPSArms.SetVisible(true);
				localPlayerAgent.Locomotion.Downed.m_owner.Inventory.AllowedToWieldItem = true;
				localPlayerAgent.Inventory.AllowedToWieldItem = true;
				return;
			}
			if (FocusStateManager.Current.m_currentState == eFocusState.Freeflight)
			{
				FocusStateManager.ChangeState(FocusStateManager.Current.m_previousFocusState, false);
			}

		}

		public static void killnearby()
        {
			PlayerAgent localPlayerAgent = PlayerManager.GetLocalPlayerAgent();
			Il2CppSystem.Collections.Generic.List<EnemyAgent> reachableEnemiesInNodes = AIG_CourseGraph.GetReachableEnemiesInNodes(localPlayerAgent.CourseNode, 20);
			for (int i = 0; i < reachableEnemiesInNodes.Count; i++)
			{
				EnemyAgent enemyAgent = reachableEnemiesInNodes[i];
				reachableEnemiesInNodes[i].Damage.FireDamage(float.MaxValue, localPlayerAgent);
				reachableEnemiesInNodes[i].Damage.FireDamage(float.MaxValue, enemyAgent);
				reachableEnemiesInNodes[i].Damage.GlueDamage(float.MaxValue);
				reachableEnemiesInNodes[i].Damage.FallDamage(float.MaxValue);
				reachableEnemiesInNodes[i].Damage.MeleeDamage(float.MaxValue, localPlayerAgent, localPlayerAgent.Position, localPlayerAgent.EyePosition);
				reachableEnemiesInNodes[i].Damage.FreezeDamage(float.MaxValue, localPlayerAgent);
				enemyAgent.Damage.FireDamage(float.MaxValue, PlayerManager.GetLocalPlayerAgent());
			}
		}

		public static void OnMeleeHitDamage()
		{
			PlayerAgent localPlayerAgent = PlayerManager.GetLocalPlayerAgent();
			if (localPlayerAgent == null)
			{
				return;
			}
			if (localPlayerAgent.Inventory == null)
			{
				return;
			}
			PlayerInventoryLocal playerInventoryLocal = localPlayerAgent.Inventory.TryCast<PlayerInventoryLocal>();
			if (playerInventoryLocal == null || playerInventoryLocal.WieldedItem == null)
			{
				return;
			}
			MeleeWeaponFirstPerson meleeWeaponFirstPerson = playerInventoryLocal.WieldedItem.TryCast<MeleeWeaponFirstPerson>();
			if (meleeWeaponFirstPerson == null || meleeWeaponFirstPerson.HitsForDamage == null || meleeWeaponFirstPerson.HitsForDamage.Count <= 0)
			{
				return;
			}
			meleeWeaponFirstPerson.SetNextDamageToDeal(eMeleeWeaponDamage.Heavy, 1E+10f);
			for (int i = 0; i < meleeWeaponFirstPerson.HitsForDamage.Count; i++)
			{
				MeleeWeaponDamageData meleeWeaponDamageData = meleeWeaponFirstPerson.HitsForDamage[i];
				if (meleeWeaponDamageData != null && meleeWeaponDamageData.damageGO != null)
				{
					Dam_EnemyDamageLimb component = meleeWeaponDamageData.damageGO.GetComponent<Dam_EnemyDamageLimb>();
					if (component != null)
					{
						Agent baseAgent = component.GetBaseAgent();
						if (baseAgent != null && baseAgent.GetComponent<EnemyAI>() != null)
						{
							EnemyAgent component2 = baseAgent.GetComponent<EnemyAgent>();
							if (component2 != null && component2.ModelRef && component2.ModelRef.m_headBone && component2.ModelRef.m_headBone.gameObject)
							{
								meleeWeaponDamageData.damageGO = component2.ModelRef.m_headBone.gameObject;
							}
						}
					}
				}
			}
		}


		public static void UpdateHitForDamage()
		{
			PlayerAgent localPlayerAgent = PlayerManager.GetLocalPlayerAgent();
			if (localPlayerAgent == null)
			{
				return;
			}
			if (localPlayerAgent.Inventory == null)
			{
				return;
			}
			PlayerInventoryLocal playerInventoryLocal = localPlayerAgent.Inventory as PlayerInventoryLocal;
			if (playerInventoryLocal == null || playerInventoryLocal.WieldedItem == null)
			{
				return;
			}
			MeleeWeaponFirstPerson meleeWeaponFirstPerson = playerInventoryLocal.WieldedItem as MeleeWeaponFirstPerson;
			if (meleeWeaponFirstPerson == null)
			{
				return;
			}
			if (meleeWeaponFirstPerson.HitsForDamage.Count <= 0)
			{
				return;
			}
			foreach (MeleeWeaponDamageData meleeWeaponDamageData in meleeWeaponFirstPerson.HitsForDamage)
			{
				Dam_EnemyDamageLimb component = meleeWeaponDamageData.damageGO.GetComponent<Dam_EnemyDamageLimb>();
				if (component != null)
				{
					Agent baseAgent = component.GetBaseAgent();
					if (baseAgent != null)
					{
						UnityEngine.Object component2 = component.GetBaseAgent().GetComponent<EnemyAI>();
						EnemyAgent enemyAgent = baseAgent as EnemyAgent;
						if (component2 != null && enemyAgent != null && enemyAgent.ModelRef && enemyAgent.ModelRef.m_headBone && enemyAgent.ModelRef.m_headBone.gameObject)
						{
							meleeWeaponDamageData.damageGO = enemyAgent.ModelRef.m_headBone.gameObject;
						}
					}
				}
			}
		}

		public static void reviveself()
		{
			PlayerAgent localPlayerAgent = PlayerManager.GetLocalPlayerAgent();
			if (localPlayerAgent != null)
			{
				//pSetHealthData data;
				SFloat16 uf = new SFloat16();
				uf.Set(25f, 25f);
				//data.health = uf;
				//localPlayerAgent.Damage.OnRevive(data);
				localPlayerAgent.Locomotion.Downed.OnPlayerRevived();
				localPlayerAgent.Damage.SendSetHealth(25f);
				return;
			}
		}

		public static void resetsentry()
		{
			PlayerAgent localPlayerAgent = PlayerManager.GetLocalPlayerAgent();
			PlayerBackpack backpack = PlayerBackpackManager.GetBackpack(localPlayerAgent.Owner);

			//PlayerBackpackManager.LocalBackpack.AmmoStorage.ClassAmmo.AddAmmo(backpack.AmmoStorage.GetBulletMaxCap(AmmoType.Class));
			PlayerBackpackManager.LocalBackpack.SetDeployed(InventorySlot.GearClass, false);

		}
		public static void Updateintensity(PlayerAgent playerAgent)
		{
			if (playerAgent.PlayerData == null)
			{
				return;
			}
			if (playerAgent.CourseNode == null)
			{
				return;
			}
			if (MusicManager.Current.m_machine.m_currentState.TryCast<MUS_CombatRegular>() == null)
			{
				return;
			}
			int num = 0;
			Il2CppSystem.Collections.Generic.List<EnemyAgent> reachableEnemiesInNodes = AIG_CourseGraph.GetReachableEnemiesInNodes(playerAgent.CourseNode, 10);
			if (reachableEnemiesInNodes != null && reachableEnemiesInNodes.Count > 0)
			{
				for (int i = 0; i < reachableEnemiesInNodes.Count; i++)
				{
					EnemyAgent enemyAgent = reachableEnemiesInNodes[i];
					if (enemyAgent != null && enemyAgent.Alive && enemyAgent.AI.Mode == AgentMode.Agressive && TrainerComponent.EnemyIsInSight(enemyAgent))
					{
						num++;
						if (enemyAgent.EnemyData.EnemyType == eEnemyType.Boss)
						{
							num += 2;
						}
						else if (enemyAgent.EnemyData.EnemyType == eEnemyType.Boss)
						{
							num++;
						}
					}
				}
			}
			if (num > 5 && DramaManager.Tension > 80f && (TrainerComponent.m_next_intensity_update == 0f || TrainerComponent.m_next_intensity_update < Clock.Time))
			{
				string eventName = "combat_cooldown_up_10";
				if (num > 8)
				{
					eventName = "combat_cooldown_up_20";
				}
				else if (num > 10)
				{
					eventName = "combat_cooldown_up_30";
				}
				else if (num > 12)
				{
					eventName = "combat_cooldown_up_40";
				}
				TrainerComponent.m_next_intensity_update = Clock.Time + 1f;
				MusicManager.Machine.Sound.Post(eventName);
			}
		}



		public static void OverrideAmmoType(PlayerBackpack playerBackpack, ItemEquippable item, float costofbulletDivider = 1f, int clipsizeMultiplier = 1)
		{
			float num = 1f;
			int num2 = 0;
			float ammoMaxCap = -1f;
			switch (item.AmmoType)
			{
				case AmmoType.Standard:
				case AmmoType.Special:
					num = ((item.ArchetypeData != null) ? item.ArchetypeData.CostOfBullet : 1f);
					num2 = item.ClipSize;
					break;
				case AmmoType.Class:
					if (item.ArchetypeData != null)
					{
						num = item.ArchetypeData.CostOfBullet * item.ItemDataBlock.ClassAmmoCostFactor;
						if ((float)item.ArchetypeData.ShotgunBulletCount > 0f)
						{
							num *= (float)item.ArchetypeData.ShotgunBulletCount;
						}
					}
					else
					{
						num = item.ItemDataBlock.ClassAmmoCostFactor;
					}
					break;
				case AmmoType.ResourcePackRel:
					num = 20f;
					break;
				case AmmoType.CurrentConsumable:
					ammoMaxCap = (float)item.ItemDataBlock.ConsumableAmmoMax;
					break;
			}
			if (num2 == 0 && clipsizeMultiplier > 1)
			{
				num2 = 1;
			}
			playerBackpack.AmmoStorage.SetupAmmoType(item.AmmoType, num / costofbulletDivider, num2 * clipsizeMultiplier, ammoMaxCap);
		}


		public static void spawnenemycroshairs(PlayerAgent localPlayerAgent)
		{
			RaycastHit raycastHit;
			if (Physics.Raycast(Camera.main.ScreenPointToRay(new Vector3((float)(Screen.width / 2), (float)(Screen.height / 2), 0f)), out raycastHit, 1000f))
			{
				Vector3 position = raycastHit.point;
				EnemyAllocator.ResetAllowedToSpawn();
				AgentMode mode = AgentMode.Hibernate;
				//uint persistantID = 37U;
				uint persistantID = 34U;
				//uint persistantID = (uint)random.Next(0, enemieslist.Length);
				if (localPlayerAgent != null && localPlayerAgent.FPSCamera.CameraRayObject != null)
				{
					position = localPlayerAgent.FPSCamera.CameraRayObject.transform.position;
				}
				EnemyAllocator.Current.SpawnEnemy(persistantID, localPlayerAgent.CourseNode, mode, position, localPlayerAgent.Rotation, null);
			}
		}


		public static void GetGameBlocksPrinted<GB>() where GB : GameDataBlockBase<GB>
		{
			if (!Directory.Exists(@"datablocks"))
			{
				Directory.CreateDirectory(@"datablocks");
			}
			File.WriteAllText($@"datablocks/{GameDataBlockBase<GB>.m_fileNameNoExt}.json", GameDataBlockBase<GB>.GetFileContents());
		}


		public static void GetAllDatablocksPrinted()
		{
			if (!TrainerComponent.PrintDataBlocks)
			{
				MelonLogger.Log("Datablocks extracted");
				return;
			}
			TrainerComponent.GetGameBlocksPrinted<ArchetypeDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<BigPickupDistributionDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<ChainedPuzzleDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<CommodityDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<ComplexResourceSetDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<ConsumableDistributionDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<CustomAssetShardDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<EffectNodeDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<EnemyBalancingDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<EnemyBehaviorDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<EnemyDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<EnemyDetectionDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<EnemyGroupDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<EnemyMovementDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<EnemyPopulationDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<EnemySFXDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<EnvironmentFeedbackDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<EventSequenceActionDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<EventSequenceDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<ExpeditionBalanceDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<ExtractionEventDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<FeedbackDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<FlashlightSettingsDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<FogScenarioDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<FogSettingsDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<GameplayTrailerDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<GearCategoryDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<GearCategoryFilterDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<GearDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<GearDecalDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<GearFlashlightPartDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<GearFrontPartDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<GearMagPartDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<GearMeleeHandlePartDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<GearMeleeHeadPartDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<GearMeleeNeckPartDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<GearMeleePommelPartDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<GearPaletteDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<GearPartAttachmentDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<GearPatternDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<GearPerkDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<GearReceiverPartDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<GearSightPartDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<GearStockPartDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<GearToolDeliveryPartDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<GearToolGripPartDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<GearToolMainPartDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<GearToolPayloadPartDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<GearToolScreenPartDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<GearToolTargetingPartDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<ItemDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<ItemFPSSettingsDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<ItemPartDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<LevelGenSettingsDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<LevelLayoutDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<LightSettingsDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<LocalizationDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<LootDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<MarkerGroupDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<MiningMarkerDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<MusicStateDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<PlayerDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<PlayerDialogDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<PlayerOfflineGearDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<RecepieDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<RecoilDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<RundownDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<ServiceMarkerDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<StaticSpawnDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<SurvivalWavePopulationDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<SurvivalWaveSettingsDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<TechMarkerDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<WardenObjectiveDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<WeaponAudioDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<WeaponDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<WeaponMuzzleFlashDataBlock>();
			TrainerComponent.GetGameBlocksPrinted<WeaponShellCasingDataBlock>();
		}

		public static void shitfoam()
		{
			PlayerAgent localPlayerAgent = PlayerManager.GetLocalPlayerAgent();
			Il2CppSystem.Collections.Generic.List<PlayerAgent> playersingame = PlayerManager.PlayerAgentsInLevel;
			foreach (PlayerAgent playerAgent in playersingame)
			{
				if (playerAgent != localPlayerAgent)
				{
					Vector3 position = playerAgent.Position;
					position.y += 1.5f;
					ProjectileManager.WantToFireGlue(playerAgent, position, Vector3.zero, 800f, true);
				}
			}
		}

		public static void ammo()
		{
			PlayerAgent localPlayerAgent = PlayerManager.GetLocalPlayerAgent();
			PlayerBackpack backpack = PlayerBackpackManager.GetBackpack(localPlayerAgent.Owner);
			PlayerAmmoStorage ammoStorage = backpack.AmmoStorage;
			Il2CppArrayBase<PlayerDataBlock> block = GameDataBlockBase<PlayerDataBlock>.GetAllBlocks();
			PlayerInventoryLocal playerInventoryLocal = localPlayerAgent.Inventory.Cast<PlayerInventoryLocal>();
			playerInventoryLocal.WieldedItem.SetCurrentClipRel(playerInventoryLocal.WieldedItem.GetMaxClip());
			playerInventoryLocal.WieldedItem.SetCurrentClip(playerInventoryLocal.WieldedItem.GetMaxClip());

			//Il2CppArrayBase<ArchetypeDataBlock> archblock = GameDataBlockBase<ArchetypeDataBlock>.GetAllBlocks();
			//foreach (ArchetypeDataBlock bll in archblock)
			//{
			//	//bll.CostOfBullet = 0f;
			//	//bll.HipFireSpread = 0f;
			//	bll.AimSpread = 0f;
			//	//bll.DefaultClipSize = 400;
			//	bll.DefaultReloadTime = 0f;
			//	//bll.EquipTransitionTime = 0f;
			//	//bll.AimTransitionTime = 0f;
			//	//bll.PiercingDamageCountLimit = 900;
			//	// bll.ShotgunBulletSpread = 0;
			//	//bll.BurstDelay = 0;
			//	bll.AimTransitionTime = 0;
			//	//bll.BurstShotCount = 100;
			//	//bll.SpecialChargetupTime = 0;
			//	//bll.SpecialCooldownTime = 0;
			//	//bll.SpecialSemiBurstCountTimeout = 0;
			//	//bll.FireMode = eWeaponFireMode.Auto;
			//	//bll.ShotDelay = 0;
			//	//bll.ShotgunBulletCount = 100;
			//	//bll.ShotgunBulletSpread = 0;
			//	//bll.ShotgunConeSize = 100;
			//	//bll.Description = "<size=%10>GTFOMenu<color=#FF6767></color>.</size>";
			//}
			if (playerInventoryLocal != null && playerInventoryLocal.WieldedItem != null && playerInventoryLocal.m_wieldedItem.CanReload && playerInventoryLocal.CanReloadCurrent() && playerInventoryLocal.WieldedItem.GetCurrentClipRel() == 0f)
			{
				playerInventoryLocal.TriggerReload();
			}

		}

		//static void test1()
		//{
		//	PlayerAgent localPlayerAgent = PlayerManager.GetLocalPlayerAgent();
		//	Il2CppSystem.Collections.Generic.List<EnemyAgent> reachableEnemiesInNodes = AIG_CourseGraph.GetReachableEnemiesInNodes(localPlayerAgent.CourseNode, 1000);
		//	for (int i = 0; i < reachableEnemiesInNodes.Count; i++)
		//	{
		//		EnemyAgent enemyAgent2 = reachableEnemiesInNodes[i];
		//		PrepareMarker(enemyAgent2.gameObject);
		//	}
		//	PrepareMarker(reachableEnemiesInNodes, reachableEnemiesInNodes.

		//}

		public static void DrawCircle(Vector2 Pos, float radius, Color color)
		{
			double pi = 3.1415926535;
			for (double num2 = 0.0; num2 < 360.0; num2 += 1.0)
			{
				double value = (double)radius * System.Math.Cos(num2 * pi / 180.0);
				double value2 = (double)radius * System.Math.Sin(num2 * pi / 180.0);
				DrawLine(new Vector2(Pos.x + Convert.ToSingle(value), Pos.y + Convert.ToSingle(value2)), new Vector2(Pos.x + 1f + Convert.ToSingle(value), Pos.y + 1f + Convert.ToSingle(value2)), color);
			}
		}

		public static void updatemembers()
		{
			if (themembers.Count > 0)
			{
				for (int i = themembers.Count - 1; i > -1; i--)
				{
					EnemyAgent enemyAgent = themembers[i];
					if (enemyAgent != null)
					{
						if (!enemyAgent.Alive)
						{
							themembers.RemoveAt(i);
						}
						if (enemyAgent.Alive && enemyAgent.IsStuck())
						{
							themembers.RemoveAt(i);
						}
					}
				}
			}
		}

		public static void gunonelevator()
		{
			Il2CppSystem.Collections.Generic.List<PlayerAgent> playersingame = PlayerManager.PlayerAgentsInLevel;
			foreach (PlayerAgent jj in playersingame)
			{
					jj.Locomotion.Downed.m_owner.FPItemHolder.FPSArms.SetVisible(true);
					jj.Locomotion.Downed.m_owner.Alive = true;
					jj.Locomotion.Downed.m_owner.Inventory.AllowedToWieldItem = true;
					jj.Inventory.AllowedToWieldItem = true;
			}
		}
		public static void despawn()
        {
			PlayerAgent localPlayerAgent = PlayerManager.GetLocalPlayerAgent();
			PlayerManager.Current.DeSpawnPlayer(localPlayerAgent);
        }

		public static void freeze()
        {
			Il2CppSystem.Collections.Generic.List<PlayerAgent> playersingame = PlayerManager.PlayerAgentsInLevel;
			foreach (PlayerAgent jj in playersingame)
			{
				if (!jj.IsLocallyOwned)
				{
					jj.FPSCamera.PlayerMoveEnabled = false;
					PlayerManager.Current.DeSpawnPlayer(jj);
					jj.gameObject.active = false;
					jj.gameObject.SetActive(false);
					jj.Alive = false;
					jj.FPItemHolder.FPSArms.SetVisible(false);
					jj.FPItemHolder.PlayerData.persistentID = 8888u;
					jj.Owner.NickName = "I HACK";
					jj.Owner.m_playerAgent.Eject();
					jj.Locomotion.ChangeState(PlayerLocomotion.PLOC_State.InElevator,true);
				}
			}

		}
		public static void kickuser(SNet_Player player)
		{

			SNet.SessionHub.SendSessionMemberChange(player, SessionMemberChangeType.Kicked, null, SNet_PlayerEventReason.Kick_ByVote);
			SNet.SessionHub.RemovePlayerFromSession(player, false);
			SNet.SessionHub.KickPlayer(player, SNet_PlayerEventReason.Kick_ByVote);
		}
		public static void togglefog()
		{
			PlayerAgent localPlayerAgent = PlayerManager.GetLocalPlayerAgent();
			FogRepeller_Sphere fogRepeller_Sphere = null;
			if (localPlayerAgent.GetComponent<FogRepeller_Sphere>() == null)
			{
				localPlayerAgent.gameObject.AddComponent<FogRepeller_Sphere>();
				fogRepeller_Sphere = localPlayerAgent.GetComponent<FogRepeller_Sphere>();
				fogRepeller_Sphere.GrowDuration = 1f;
				fogRepeller_Sphere.ShrinkDuration = 1f;
				fogRepeller_Sphere.Density = -500f;
				fogRepeller_Sphere.InfiniteDuration = true;
			}
			if (fogRepeller_Sphere == null)
			{
				fogRepeller_Sphere = localPlayerAgent.GetComponent<FogRepeller_Sphere>();
			}
			if (fogRepeller_Sphere.CurrentState == eFogRepellerSphereState.Disabled || fogRepeller_Sphere.CurrentState == eFogRepellerSphereState.Shrink)
			{
				fogRepeller_Sphere.m_infectionShield = new EV_Sphere();
				fogRepeller_Sphere.m_infectionShield.maxRadius = 0f;
				fogRepeller_Sphere.m_infectionShield.minRadius = 0f;
				fogRepeller_Sphere.Range = 50f;
				fogRepeller_Sphere.StartRepelling();
				return;
			}
			fogRepeller_Sphere.StopRepelling();
			return;
		}



			public static void IncrementData()
			{
				CurrentExpInt++;
				if (CurrentExpInt > list.Count - 1)
				{
					CurrentExpInt = 0;
				}
				OnSelectExpidition();
			}
			public static void DecrementData()
			{
				CurrentExpInt--;
				if (CurrentExpInt < 0)
				{
					CurrentExpInt = list.Count - 1;
				}
				OnSelectExpidition();
			}

			public static void OnSelectExpidition()
			{
				int currentExpInt = CurrentExpInt;
				PlayerAgent localPlayerAgent = PlayerManager.GetLocalPlayerAgent();
				if (localPlayerAgent != null)
				{
					RundownManager.MasterSelectActiveExpedition(eRundownKey.Local, list[currentExpInt].tier, list[currentExpInt].index, "Local", list[currentExpInt].RundownID);
					PlayerChatManager.WantToSentTextMessage(localPlayerAgent, "");
					PlayerChatManager.WantToSentTextMessage(localPlayerAgent, "   RundownID: <color=yellow>" + list[currentExpInt].RundownID.ToString() + "</color>");
					PlayerChatManager.WantToSentTextMessage(localPlayerAgent, "   Selecting: " + list[currentExpInt].name);
					PlayerChatManager.WantToSentTextMessage(localPlayerAgent, "   Tier: <color=green>" + list[currentExpInt].tier.ToString() + "</color>");
					PlayerChatManager.WantToSentTextMessage(localPlayerAgent, "   Index: " + list[currentExpInt].index.ToString());
				}
			}
		public static void CheckInputs(PlayerAgent localPlayerAgent)
		{

			if (Input.GetKey(KeyCode.KeypadPlus))
			{
				DecrementData();
				return;
			}
			if (Input.GetKeyDown(KeyCode.KeypadMinus))
			{
				IncrementData();
				return;
			}
			if (bspawn)
			{
				if (Input.GetKeyDown(KeyCode.Mouse2))
				{
					spawnenemycroshairs(PlayerManager.GetLocalPlayerAgent());
				}
				return;
			}

			if (Input.GetKeyDown(KeyCode.Space))
			{
				if (bjump)
				{
					Highjump(30f);
					return;
				}
				else
					Highjump(0f);
				return;
			}
			if (Input.GetKeyDown(KeyCode.W))
			{
				fastrun();
				return;
			}
			if (Input.GetKeyDown(KeyCode.Insert))
			{
				openmenu = !openmenu;
				ShowCursor = !ShowCursor;
				CursorLockMode lockState = Cursor.lockState;
				CursorLockMode cursorLockMode = lockState;
				if (cursorLockMode != CursorLockMode.None)
				{
					if (cursorLockMode == CursorLockMode.Locked)
					{
						fsm = FocusStateManager.CurrentState;
						FocusStateManager.ChangeState(eFocusState.None);
						Cursor.lockState = CursorLockMode.None;
						Cursor.visible = true;
					}
					return;
				}
				else
				{
					Cursor.lockState = CursorLockMode.Locked;
					FocusStateManager.ChangeState(fsm);
					Cursor.visible = false;

				}
				return;
			}
			if (Input.GetKeyDown(KeyCode.F2))
			{
				freecam();
				return;
			}
			if (Input.GetKeyDown(KeyCode.F3))
			{
				telebadguys();
				return;
			}
			if (Input.GetKeyDown(KeyCode.F4))
			{
				//teleme();
				downplayer();
				return;
			}
			if (Input.GetKeyDown(KeyCode.F5))
			{
				openthing();
				


				//enemydraw();
				//ammo();
				//spawnitems2();
				//TrainerComponent.DecreaseCostofBullets(InventorySlot.GearStandard, 3f, 1);
				return;
			}
			if (Input.GetKeyDown(KeyCode.F6))
			{
				killincrosshairs();
				//troll();
				//TrainerComponent.DecreaseCostofBullets(InventorySlot.GearSpecial, 4f, 1);

				return;
			}
			if (Input.GetKeyDown(KeyCode.F7))
			{
				killnearby();
				return;





				//ScreenLiquidJob screenLiquidJob = new ScreenLiquidJob();
				//screenLiquidJob.dir = Vector3.up;
				//screenLiquidJob.isDirect = true;
				//screenLiquidJob.pos = Vector3.up;
				//screenLiquidJob.setting = ScreenLiquidSettingName.playerBlood_Downed;
				//ScreenLiquidSetting liquid = new ScreenLiquidSetting();
				//liquid.particleCount = 100;
				//liquid.colorA = Color.green;
				//liquid.colorB = Color.red;
				//liquid.rndOffsetStart = new Vector2(UnityEngine.Random.Range(-5.0f, 5.0f), UnityEngine.Random.Range(-5.0f, 5.0f));
				//liquid.rndOffsetEnd = new Vector2(UnityEngine.Random.Range(5f, 10f), UnityEngine.Random.Range(5f, 10f));
				//liquid.shapeStart = new Vector2(UnityEngine.Random.Range(1, 10), UnityEngine.Random.Range(1, 10));
				//liquid.shapeEnd = new Vector2(UnityEngine.Random.Range(5, 10), UnityEngine.Random.Range(5, 10));
				//liquid.waterMin = UnityEngine.Random.Range(5, 10);
				//liquid.waterMax = UnityEngine.Random.Range(40, 100);
				//liquid.dirScale = UnityEngine.Random.Range(1, 10);
				//liquid.appliation = ScreenLiquidApplication.Splash;

				//ScreenLiquidSettingsObject screenLiquidSettingsObject = ScreenLiquidManager.settings[(int)screenLiquidJob.setting];
				//Il2CppSystem.Collections.Generic.List<PlayerAgent> playersingame = PlayerManager.PlayerAgentsInLevel;
				//foreach (PlayerAgent jj in playersingame)
				//{
				//	if (jj.IsLocallyOwned)
				//	{






				//		//jj.Locomotion.Downed.Enter();
				//		//jj.Locomotion.Downed.Exit();


				//		//jj.FPSCamera.PlayerMoveEnabled = false;
				//		


				//		//jj.FPSCamera.GlassLiquids.Splat(ref screenLiquidJob, ref liquid);
				//		//jj.FPSCamera.GlassLiquids.Splat(ref screenLiquidJob, ref liquid);
				//		//jj.FPSCamera.GlassLiquids.Splat(ref screenLiquidJob, ref liquid);
				//		//jj.FPSCamera.GlassLiquids.Splat(ref screenLiquidJob, ref liquid);
				//		//jj.FPSCamera.GlassLiquids.Splat(ref screenLiquidJob, ref liquid);
				//		//jj.FPSCamera.GlassLiquids.Splat(ref screenLiquidJob, ref liquid);



				//		//GuiManager.PlayerLayer.m_wardenObjective.m_pressBtn_Text.SetText(texttoequip);

				//		//GuiManager.InteractionLayer.SetMessage("<b>YOU ARE DOWNED, WAITING FOR TEAMMATE! (NOTE: 'TAB' TO OPEN MAP)</b>", ePUIMessageStyle.Default, 0);


				//	}





				//}
			}
			if (Input.GetKeyDown(KeyCode.F8))
			{
				//NoClip();
				Completelevel();
				//TrainerComponent.DecreaseCostofBullets(InventorySlot.ResourcePack, 1f, 1);
				return;
			}
			if (Input.GetKeyDown(KeyCode.F9))
			{
				shitfoam();
				healmedamnit();

				return;
			}
			if (Input.GetKeyDown(KeyCode.F10))
			{
				
				//DamageUtil.DoExplosionDamage(localPlayerAgent.Position, 100f, 1000f, LayerManager.MASK_EXPLOSION_TARGETS, LayerManager.MASK_EXPLOSION_BLOCKERS, true, 100f);
				//CellSound.Post(0xD61E0B45U, localPlayerAgent.Position);

				return;
			}
			if (Input.GetKeyDown(KeyCode.F11))
			{
					PlayerVoiceManager.WantToSayAndStartDialog(localPlayerAgent.CharacterID, EVENTS.PLAY_SNEEZE01, EVENTS.PLAY_SNEEZENOTTENSE01_2B);
					PlayerVoiceManager.WantToSayAndStartDialog(localPlayerAgent.CharacterID, EVENTS.PLAY_SNEEZENOTTENSE01_2B, 0);
					PlayerVoiceManager.WantToSayAndStartDialog(localPlayerAgent.CharacterID, EVENTS.PLAY_SNEEZETENSE01_1A, EVENTS.PLAY_SNEEZETENSE01_2B);

					//	gunonelevator();
					return;
			}

		}


		#region harmonypatches

	


		[HarmonyPatch(typeof(Dam_PlayerDamageBase), "ReceiveAddHealth", new Type[]
{
		typeof(pAddHealthData),
})]
		public class Dam_PlayerDamageBase_ReceiveAddHealth
		{
			[HarmonyPrefix]
			public static void Prefix(Dam_PlayerDamageBase __instance, ref pAddHealthData data)
			{

				MelonLogger.Log("ReceiveAddHealth2: " + data.health.internalValue);
				//SNet.IsMaster = true;

			}
		}


		[HarmonyPatch(typeof(Dam_PlayerDamageBase), "ReceiveSetHealth", new Type[]
{
		typeof(pSetHealthData),
})]
		public class Dam_PlayerDamageBase_ReceiveSetHealth
		{
			[HarmonyPrefix]
			public static void Prefix(Dam_PlayerDamageBase __instance, ref pSetHealthData data)
			{
				MelonLogger.Log("ReceiveSetHealth2: " + data.health.internalValue);
				//SNet.IsMaster = true;

			}
		}






		//[HarmonyPatch(typeof(GlobalSetup), "Setup")]
		//public class GlobalSetup_Setup
		//{
		//	[HarmonyPrefix]
		//	public static void Prefix(GlobalSetup __instance)
		//	{
		//		__instance.m_skipIntro = true;
		//		__instance.m_showStartupScreen = false;
		//	}
		//}
		//[HarmonyPatch(typeof(CM_PageIntro), "Setup")]
		//public class CM_PageIntro_Setup
		//{
		//	public static void Postfix(CM_PageIntro __instance)
		//	{
		//		__instance.EXT_PressInject(0);
		//	}
		//}
		//[HarmonyPatch(typeof(CM_PageRundown_New), "Setup")]
		//public class CM_PageRundown_New_Setup
		//{
		//	public static void Prefix(CM_PageRundown_New __instance)
		//	{
		//		__instance.m_cortexIntroIsDone = true;
		//		__instance.m_rundownIntroIsDone = true;
		//	}
		//}


		//[HarmonyPatch(typeof(CM_TimedButton), "OnHoverUpdate")]
		//public class CM_TimedButton_OnHoverUpdate
		//{
		//	public static bool Prefix(CM_TimedButton __instance, ref iCellMenuInputHandler inputHandler)
		//	{
		//		if (!inputHandler.MainButtonStatus)
		//			return false;
		//		__instance.DoBtnPress(inputHandler);
		//		__instance.m_holdBtnActive = false;
		//		CM_PageBase.PostSound(__instance.SOUND_CLICK_HOLD_DONE, "Click hold DONE");
		//		return false;
		//	}
		//}




		//        [HarmonyPatch(typeof(Dam_SyncedDamageBase), "ReceiveSetHealth", new Type[]
		//{
		//                typeof(pSetHealthData),
		//})]
		//        public class Dam_SyncedDamageBase_ReceiveSetHealth
		//        {
		//            [HarmonyPrefix]
		//            public static void Prefix(Dam_SyncedDamageBase __instance, ref pSetHealthData data)
		//            {
		//				MelonLogger.Log("ReceiveSetHealth3: " + data.health.internalValue);
		//				//SNet.IsMaster = true;

		//			}
		//        }



		//        [HarmonyPatch(typeof(Dam_SyncedDamageBase), "ReceiveAddHealth", new Type[]
		//{
		//                typeof(pAddHealthData)
		//})]
		//        public class Dam_SyncedDamageBase_ReceiveAddHealth
		//        {
		//            [HarmonyPrefix]
		//            public static void Prefix(Dam_SyncedDamageBase __instance, ref pAddHealthData data)

		//            {
		//                MelonLogger.Log("ReceiveSetHealth4: " + data.health.internalValue);
		//                SNet.IsMaster = true;
		//            }
		//        }

















		[HarmonyPatch(typeof(PlayerManager), "DeSpawnPlayer", new Type[]
{
		typeof(PlayerAgent),
})]
		public class PlayerManager_DeSpawnPlayer
		{
			[HarmonyPrefix]
			public static void Prefix(PlayerManager __instance, ref PlayerAgent agent)
			{
				SNet.IsMaster = true;
			}
		}



		[HarmonyPatch(typeof(EnemyAllocator), "OnEnemySpawned", new Type[]
{
		typeof(pEnemySpawnData),
		typeof(EnemyReplicator)
})]
		public class EnemyAllocator_OnEnemySpawned
		{
			[HarmonyPrefix]
			public static void Prefix(EnemyAllocator __instance, ref pEnemySpawnData spawnData, ref EnemyReplicator replicator)
			{
				EnemyAgent component = replicator.ReplicatorSupplier.gameObject.GetComponent<EnemyAgent>();
				component.gameObject.transform.localScale = (component.gameObject.transform.localScale + new Vector3(sizemulf, sizemulf, sizemulf));
			}
		}



		//[HarmonyPatch(typeof(GameStateManager), "IsEveryoneReady")]
		//public class GameStateManager_IsEveryoneReady
		//{
		//	[HarmonyPostfix]
		//	public static void Postfix(ref bool __result)
		//	{
		//		__result = true;
		//	}
		//}

		[HarmonyPatch(typeof(BulletWeapon), "Fire", new Type[]
{
		typeof(bool),
})]
		public class BulletWeapon_fire
		{
			[HarmonyPostfix]
			public static void Postfix(BulletWeapon __instance, ref bool resetRecoilSimilarity)
			{
				if (bammo)
				{
					ammo();
				}


			}
		}
		//
		//SentryGunInstance_Firing_Bullets.UpdateAmmo(int) : void @06001376
		[HarmonyPatch(typeof(SentryGunInstance_Firing_Bullets), "UpdateAmmo", new Type[]
{
		typeof(int),
})]
		public class SentryGunInstance_Firing_Bullets_UpdateAmmo
		{
			[HarmonyPrefix]
			public static void Prefix(SentryGunInstance_Firing_Bullets __instance, ref int bullets)
			{
				if (bammo && bullets < 0)
				{
					bullets = 0;
				}


			}
		}
		//Player.PlayerAmmoStorage.UpdateBulletsInPack(AmmoType, int) : float @0600344E
		[HarmonyPatch(typeof(PlayerAmmoStorage), "UpdateBulletsInPack", new Type[]
{
		typeof(AmmoType),
		typeof(int),
})]
		public class PlayerAmmoStorage_UpdateBulletsInPack
		{
			[HarmonyPrefix]
			public static void Prefix(PlayerAmmoStorage __instance, ref AmmoType ammoType, ref int bulletCount)
			{
				if (bammo && bulletCount < 0)
				{
					bulletCount = 0;
				}


			}
		}


		[HarmonyPatch(typeof(Shotgun), "Fire", new Type[]
{
		typeof(bool),
})]
		public class Shotgun_fire
		{
			[HarmonyPostfix]
			public static void Postfix(BulletWeapon __instance, ref bool resetRecoilSimilarity)
			{
				if (bammo)
				{
					ammo();
				}


			}
		}


		[HarmonyPatch(typeof(BulletWeapon), "Fire", new Type[]
{
		typeof(bool),
})]
		public class BulletWeapon_fire_pre
		{
			[HarmonyPrefix]
			public static void Prefix(BulletWeapon __instance, ref bool resetRecoilSimilarity)
			{
				if (bammo)
				{
					ammo();
				}


			}
		}

		[HarmonyPatch(typeof(Shotgun), "Fire", new Type[]
{
		typeof(bool),
})]
		public class Shotgun_fire_pre
		{
			[HarmonyPrefix]
			public static void Prefix(BulletWeapon __instance, ref bool resetRecoilSimilarity)
			{
				if (bammo)
				{
					ammo();
				}


			}
		}


		[HarmonyPatch(typeof(Cursor), "set_lockState")]
		public class Cursor_set_Lockstate
		{
			[HarmonyPrefix]
			public static void Prefix(ref CursorLockMode value)
			{
				bool showCursor = ShowCursor;
				if (showCursor)
				{
					value = CursorLockMode.None;
					Cursor.visible = true;
				}
				else
				{
					value = CursorLockMode.Locked;
					Cursor.visible = false;
				}
			}
		}

		[HarmonyPatch(typeof(Cursor), "set_visible")]
		public class Cursor_set_visible
		{
			[HarmonyPrefix]
			public static void Prefix(ref bool value)
			{
				bool showCursor = ShowCursor;
				if (showCursor)
				{
					value = true;
				}
				else
				{
					value = false;
				}
			}
		}

		[HarmonyPatch(typeof(EnemyGroup), "RegisterMember", new Type[]
{
		typeof(EnemyAgent),
})]
		public class EnemyGroup_registermember
		{
			[HarmonyPostfix]
			public static void Postfix(EnemyGroup __instance, ref EnemyAgent enemyAgent)
			{
				bool spawncrazy = true;
				themembers.Add(enemyAgent);
			}
			public static void Prefix(EnemyGroup __instance, ref EnemyAgent enemyAgent)
			{
				bool spawncrazy = true;
				if (spawncrazy)
				{
					enemyAgent.AI.SetStartMode(AgentMode.Agressive);
					enemyAgent.EnemyData.EnemyType = eEnemyType.Special;
					//enemyAgent.Locomotion.ChangeState(ES_StateEnum.Hitreact);
					enemyAgent.AI.Mode = AgentMode.Agressive;
					enemyAgent.AI.SetStartMode(AgentMode.Agressive);

				}
			}

		}

		[HarmonyPatch(typeof(PLOC_Downed), "CommonEnter")]
		public class PLOC_Downed_CommonEnter
		{
			[HarmonyPostfix]
			public static void Postfix(PLOC_Downed __instance)
			{
				__instance.m_owner.Inventory.AllowedToWieldItem = true;
				
			}
		}



		//Player.PlayerManager.RegisterPlayerAgent(PlayerAgent) : void @060032F9
		[HarmonyPatch(typeof(PlayerManager), "RegisterPlayerAgent", new Type[]
{
		typeof(PlayerAgent),
})]
		public class PlayerManager_RegisterPlayerAgent_namespoof
		{
			[HarmonyPrefix]
			public static void Prefix(ref PlayerAgent agent)
			{
				bool isLocallyOwned = agent.IsLocallyOwned;
				if (isLocallyOwned && namespoof != "")
				{
					agent.Owner.NickName = namespoof;
				}			}
		}

		[HarmonyPatch(typeof(ScreenLiquidManager), "Update")]
		public class ScreenLiquidManager_Update
		{
			[HarmonyPrefix]
			public static void Prefix(ScreenLiquidManager __instance)
			{
				if (bgodmode)
				{
					ScreenLiquidManager.Clear();
					return;
				}
			}
			[HarmonyPostfix]
			public static void Postfix(ScreenLiquidManager __instance)
			{
				if (bgodmode)
				{
					ScreenLiquidManager.Clear();
					return;
				}
			}
		}


		[HarmonyPatch(typeof(Dam_PlayerDamageBase), "ModifyInfection", new Type[] { typeof(pInfection), typeof(bool), typeof(bool) })]
		public class Dam_PlayerDamageBase_ModifyInfection
		{
			[HarmonyPrefix]
			public static void Prefix(Dam_PlayerDamageBase __instance, ref pInfection data, ref bool sync, ref bool updatePageMap)
			{
				if (bgodmode && __instance.Owner.IsLocallyOwned)
				{
					data.amount = 0;
					healmedamnit();
				}
			}
		}


		[HarmonyPatch(typeof(CM_TimedButton), "SetButtonEnabled")]
		public class CM_TimedButton_SetButtonEnabled
		{
			[HarmonyPrefix]
			public static void Prefix(ref bool enabled)
			{
				enabled = true;
			}
		}


		[HarmonyPatch(typeof(Dam_PlayerDamageBase), "OnIncomingDamage", new Type[]
{
		typeof(float),
		typeof(bool)
})]
		public class Dam_PlayerDamageBase_OnIncomingDamage
		{
			[HarmonyPrefix]
			public static void Prefix(Dam_PlayerDamageBase __instance, ref float damage, ref bool triggerDialog)
			{
				PlayerAgent localPlayerAgent = PlayerManager.GetLocalPlayerAgent();
				localPlayerAgent.Damage.IgnoreAllDamage = false;
				localPlayerAgent.Damage.CannotDie = false;
				MelonLogger.Log("Name: " + __instance.Owner.PlayerName + " Damage: " + damage);
				if (bgodmode && __instance.Owner.PlayerName == localPlayerAgent.PlayerName)
				{
					MelonLogger.Log("Name: " + __instance.Owner.PlayerName + " Damage: " + damage + " WITH GOD");
					damage = 0f;
					triggerDialog = false;
					__instance.CannotDie = true;
					__instance.IgnoreAllDamage = true;
					localPlayerAgent.Damage.IgnoreAllDamage = true;
					localPlayerAgent.Damage.CannotDie = true;
					GuiManager.PlayerLayer.m_playerStatus.m_healthText.SetText("#GOD#");
					//healmedamnit();
					return;
				}
			}
		}

		[HarmonyPatch(typeof(Dam_SyncedDamageBase), "AddHealth", new Type[] { typeof(float), typeof(Agent) })]
		public class Dam_SyncedDamageBase_patch
		{
			[HarmonyPrefix]
			public static void Prefix(Dam_SyncedDamageBase __instance, ref float addHealth, ref Agent sourceAgent)
			{
				if (bgodmode && addHealth < 0.0f && sourceAgent.IsLocallyOwned)
				{
					MelonLogger.Log("AddHealth GOD: " + bgodmode + " Amount: " + addHealth + " LocalOwn: " + sourceAgent.IsLocallyOwned);
					//healmedamnit();
					addHealth = 0f;
					GuiManager.PlayerLayer.m_playerStatus.m_healthText.SetText("#GOD#");
				}
			}
		}

		//[HarmonyPatch(typeof(Dam_SyncedDamageBase), "TentacleAttackDamage", new Type[] { typeof(float), typeof(Agent), typeof(Vector3) })]
		//public class Dam_SyncedDamageBase_TentacleAttackDamage
		//{
		//	[HarmonyPrefix]
		//	public static void Prefix(Dam_SyncedDamageBase __instance, ref float dam, ref Agent sourceAgent, ref Vector3 position)
		//	{
		//		PlayerAgent localPlayerAgent = PlayerManager.GetLocalPlayerAgent();
		//		if (bgodmode && sourceAgent.IsLocallyOwned)
		//		{
		//			dam = 0f;
		//			GuiManager.PlayerLayer.m_playerStatus.m_healthText.SetText("#GOD#");
		//			localPlayerAgent.Damage.SendSetHealth(localPlayerAgent.Damage.HealthMax);
		//			localPlayerAgent.Damage.Health = localPlayerAgent.Damage.HealthMax;
		//			localPlayerAgent.Damage.AddHealth(localPlayerAgent.Damage.HealthMax - localPlayerAgent.Damage.Health, localPlayerAgent);
		//			healmedamnit();
		//		}
		//	}
		//}

		//[HarmonyPatch(typeof(Dam_SyncedDamageBase), "ShooterProjectileDamage", new Type[] { typeof(float), typeof(Vector3) })]
		//public class Dam_SyncedDamageBase_ShooterProjectileDamage
		//{
		//	[HarmonyPrefix]
		//	public static void Prefix(Dam_SyncedDamageBase __instance, ref float dam, ref Vector3 position)
		//	{
		//		PlayerAgent localPlayerAgent = PlayerManager.GetLocalPlayerAgent();
		//		if (bgodmode)
		//		{
		//			healmedamnit();
		//			GuiManager.PlayerLayer.m_playerStatus.m_healthText.SetText("#GOD#");
		//			localPlayerAgent.Damage.SendSetHealth(localPlayerAgent.Damage.HealthMax);
		//			localPlayerAgent.Damage.Health = localPlayerAgent.Damage.HealthMax;
		//			localPlayerAgent.Damage.AddHealth(localPlayerAgent.Damage.HealthMax - localPlayerAgent.Damage.Health, localPlayerAgent);
		//			dam = 0f;
		//		}
		//	}
		//}

		//[HarmonyPatch(typeof(Dam_SyncedDamageBase), "TentacleTankGrabDamage", new Type[] { typeof(float), typeof(Agent) })]
		//public class Dam_SyncedDamageBase_ReceiveTentacleTankGrabDamage
		//{
		//	[HarmonyPrefix]
		//	public static void Prefix(Dam_SyncedDamageBase __instance, ref float dam, ref Agent sourceAgent)
		//	{
		//		PlayerAgent localPlayerAgent = PlayerManager.GetLocalPlayerAgent();
		//		if (bgodmode)
		//		{
		//			healmedamnit();
		//			GuiManager.PlayerLayer.m_playerStatus.m_healthText.SetText("#GOD#");
		//			localPlayerAgent.Damage.SendSetHealth(localPlayerAgent.Damage.HealthMax);
		//			localPlayerAgent.Damage.Health = localPlayerAgent.Damage.HealthMax;
		//			localPlayerAgent.Damage.AddHealth(localPlayerAgent.Damage.HealthMax - localPlayerAgent.Damage.Health, localPlayerAgent);
		//			dam = 0f;
		//		}
		//	}
		//}

		//[HarmonyPatch(typeof(Dam_SyncedDamageBase), "ParasiteTrapDamage", new Type[] { typeof(float), typeof(uint) })]
		//public class Dam_SyncedDamageBase_ReceiveTentacleTrapGrabDamage
		//{
		//	[HarmonyPrefix]
		//	public static void Prefix(Dam_SyncedDamageBase __instance, ref float dam, ref uint staticEnemyID)
		//	{
		//		PlayerAgent localPlayerAgent = PlayerManager.GetLocalPlayerAgent();
		//		if (bgodmode)
		//		{
		//			healmedamnit();
		//			GuiManager.PlayerLayer.m_playerStatus.m_healthText.SetText("#GOD#");
		//			localPlayerAgent.Damage.SendSetHealth(localPlayerAgent.Damage.HealthMax);
		//			localPlayerAgent.Damage.Health = localPlayerAgent.Damage.HealthMax;
		//			localPlayerAgent.Damage.AddHealth(localPlayerAgent.Damage.HealthMax - localPlayerAgent.Damage.Health, localPlayerAgent);
		//			dam = 0f;
		//		}
		//	}
		//}

		//[HarmonyPatch(typeof(Dam_SyncedDamageBase), "TentacleGrabStop")]
		//public class Dam_SyncedDamageBase_TentacleGrabStop
		//{
		//	[HarmonyPrefix]
		//	public static void Prefix(Dam_SyncedDamageBase __instance)
		//	{
		//		UFloat16 uf = new UFloat16();
		//		uf.Set(0f, 0f);
		//		pMiniDamageData data = default(pMiniDamageData);
		//		PlayerAgent localPlayerAgent = PlayerManager.GetLocalPlayerAgent();
		//		if (bgodmode)
		//		{
		//			healmedamnit();
		//			GuiManager.PlayerLayer.m_playerStatus.m_healthText.SetText("#GOD#");
		//			localPlayerAgent.Damage.SendSetHealth(localPlayerAgent.Damage.HealthMax);
		//			localPlayerAgent.Damage.Health = localPlayerAgent.Damage.HealthMax;
		//			localPlayerAgent.Damage.AddHealth(localPlayerAgent.Damage.HealthMax - localPlayerAgent.Damage.Health, localPlayerAgent);
		//			data.damage = uf;
		//		}
		//	}
		//}

		//[HarmonyPatch(typeof(Dam_SyncedDamageBase), "FallDamage", new Type[] { typeof(float)})]
		//public class Dam_SyncedDamageBase_FallDamage
		//{
		//	[HarmonyPrefix]
		//	public static void Prefix(Dam_SyncedDamageBase __instance, ref float dam)
		//	{
		//		PlayerAgent localPlayerAgent = PlayerManager.GetLocalPlayerAgent();
		//		if (bgodmode)
		//		{
		//			healmedamnit();
		//			GuiManager.PlayerLayer.m_playerStatus.m_healthText.SetText("#GOD#");
		//			localPlayerAgent.Damage.SendSetHealth(localPlayerAgent.Damage.HealthMax);
		//			localPlayerAgent.Damage.Health = localPlayerAgent.Damage.HealthMax;
		//			localPlayerAgent.Damage.AddHealth(localPlayerAgent.Damage.HealthMax - localPlayerAgent.Damage.Health, localPlayerAgent);
		//			dam = 0f;
		//		}
		//	}
		//}

		//[HarmonyPatch(typeof(Dam_SyncedDamageBase), "NoAirDamage", new Type[] { typeof(float) })]
		//public class Dam_SyncedDamageBase_NoAirDamage
		//{
		//	[HarmonyPrefix]
		//	public static void Prefix(Dam_SyncedDamageBase __instance, ref float dam)
		//	{
		//		PlayerAgent localPlayerAgent = PlayerManager.GetLocalPlayerAgent();
		//		if (bgodmode)
		//		{
		//			healmedamnit();
		//			GuiManager.PlayerLayer.m_playerStatus.m_healthText.SetText("#GOD#");
		//			localPlayerAgent.Damage.SendSetHealth(localPlayerAgent.Damage.HealthMax);
		//			localPlayerAgent.Damage.Health = localPlayerAgent.Damage.HealthMax;
		//			localPlayerAgent.Damage.AddHealth(localPlayerAgent.Damage.HealthMax - localPlayerAgent.Damage.Health, localPlayerAgent);
		//			dam = 0f;
		//		}
		//	}
		//}

		//[HarmonyPatch(typeof(Dam_SyncedDamageBase), "ParasiteDamage", new Type[] { typeof(float) })]
		//public class Dam_SyncedDamageBase_ParasiteDamage
		//{
		//	[HarmonyPrefix]
		//	public static void Prefix(Dam_SyncedDamageBase __instance, ref float dam)
		//	{
		//		PlayerAgent localPlayerAgent = PlayerManager.GetLocalPlayerAgent();
		//		if (bgodmode)
		//		{
		//			healmedamnit();
		//			GuiManager.PlayerLayer.m_playerStatus.m_healthText.SetText("#GOD#");
		//			localPlayerAgent.Damage.SendSetHealth(localPlayerAgent.Damage.HealthMax);
		//			localPlayerAgent.Damage.Health = localPlayerAgent.Damage.HealthMax;
		//			localPlayerAgent.Damage.AddHealth(localPlayerAgent.Damage.HealthMax - localPlayerAgent.Damage.Health, localPlayerAgent);
		//			dam = 0f;
		//		}
		//	}
  //      }


   //     [HarmonyPatch(typeof(Dam_SyncedDamageBase), "RegisterDamage", new Type[] { typeof(float) })]
   //     public class Dam_SyncedDamageBase_RegisterDamage
   //     {
   //         [HarmonyPrefix]
   //         public static void Prefix(Dam_SyncedDamageBase __instance, ref float dam)
   //         {
			//	PlayerAgent localPlayerAgent = PlayerManager.GetLocalPlayerAgent();
			//	if (__instance.DamageBaseOwner == DamageBaseOwnerType.Player && bgodmode)
			//		{
			//		healmedamnit();
			//		GuiManager.PlayerLayer.m_playerStatus.m_healthText.SetText("#GOD#");
			//		localPlayerAgent.Damage.SendSetHealth(localPlayerAgent.Damage.HealthMax);
			//		localPlayerAgent.Damage.Health = localPlayerAgent.Damage.HealthMax;
			//		localPlayerAgent.Damage.AddHealth(localPlayerAgent.Damage.HealthMax - localPlayerAgent.Damage.Health, localPlayerAgent);
			//		dam = 0f;
			//	}
			//}
        //}


        [HarmonyPatch(typeof(FPSCamera), "AddHitReact", new Type[]
		{
		typeof(float),
		typeof(Vector3),
		typeof(float),
		typeof(bool),
		typeof(bool)
		})]
		public class FPSCamera_AddHitReact
		{
			[HarmonyPrefix]
			public static bool Prefix()
			{
				bool godMode = bgodmode;
				return !godMode;
			}
		}

		[HarmonyPatch(typeof(MWS_AttackSwingBase), "Update")]
		public class Patch_MWS_AttackSwingBase
		{
			public static void Postfix(MWS_AttackSwingBase __instance)
			{
				if (__instance.m_weapon.HitsForDamage != null && __instance.m_weapon.HitsForDamage.Count > 0)
				{
					for (int i = 0; i < __instance.m_weapon.HitsForDamage.Count; i++)
					{
						MeleeWeaponDamageData meleeWeaponDamageData = __instance.m_weapon.HitsForDamage[i];
						if (meleeWeaponDamageData != null && meleeWeaponDamageData.damageGO != null)
						{
							Dam_EnemyDamageLimb component = meleeWeaponDamageData.damageGO.GetComponent<Dam_EnemyDamageLimb>();
							if (component != null)
							{
								Agent baseAgent = component.GetBaseAgent();
								if (baseAgent != null && baseAgent.GetComponent<EnemyAI>() != null)
								{
									EnemyAgent component2 = baseAgent.GetComponent<EnemyAgent>();
									if (component2 != null && component2.ModelRef && component2.ModelRef.m_headBone && component2.ModelRef.m_headBone.gameObject)
									{
										meleeWeaponDamageData.damageGO = component2.ModelRef.m_headBone.gameObject;
									}
								}
							}
						}
					}
				}
			}
		}

		[HarmonyPatch(typeof(PlayerAgent), "TriggerMarkerPing")]
		public class Patch_TriggerMarkerPing
		{
			public static void Prefix(ref iPlayerPingTarget target, ref Vector3 worldPos)
			{
				if (target != null)
				{
					eNavMarkerStyle pingTargetStyle = target.PingTargetStyle;
					if (pingTargetStyle == eNavMarkerStyle.PlayerPingResourceBox || pingTargetStyle == eNavMarkerStyle.PlayerPingResourceLocker)
					{
						Vector3 vector = Vector3.zero;
						Collider[] array = Physics.OverlapSphere(worldPos, 1f, LayerManager.MASK_GIVE_RESOURCE_PACK, QueryTriggerInteraction.Ignore);
						float num = -1f;
						foreach (Collider collider in array)
						{
							if (collider)
							{
								ResourcePackPickup component = collider.gameObject.GetComponent<ResourcePackPickup>();
								PlayerPingTarget component2 = collider.GetComponent<PlayerPingTarget>();
								if (component != null && component2 != null)
								{
									float num2 = Vector3.Distance(worldPos, collider.transform.position);
									if ((num == -1f || num2 < num) && (component.m_packType == eResourceContainerSpawnType.AmmoWeapon || component.m_packType == eResourceContainerSpawnType.AmmoTool || component.m_packType == eResourceContainerSpawnType.Health))
									{
										pingTargetStyle = component2.PingTargetStyle;
										target = component2.TryCast<iPlayerPingTarget>();
										vector = collider.transform.position;
										num = num2;
									}
								}
							}
						}
						if (vector != Vector3.zero)
						{
							worldPos = vector;
							target.PingTargetStyle = pingTargetStyle;
						}
					}
				}
			}
		}

		[HarmonyPatch(typeof(StartMainGame), "Start")]
		public class Patch_StartMainGame
		{
			public static void Postfix()
			{
				gtfoloader.Current.OnMainGameStart();
			}
		}           

		[HarmonyPatch(typeof(PlayFabManager), "TryGetStartupScreenData")]
		public class Patch_PlayFabManager
		{
			public static void Postfix(PlayFabManager __instance, eStartupScreenKey key, out StartupScreenData data)
			{
				data = new StartupScreenData();
				data.ShowDiscordButton = false;
				data.ShowBugReportButton = false;
				data.ShowRoadmapButton = false;
				data.IntroText = "<size=%50>GTFOMenu Loaded<color=#dff20a>" + "\n" + "\n" + "[DEL] for PREGAME menu" + "</color></size>" +"\n" + "\n" + "<size=%50><color=#c71a34>Join Discord (Press F9)" + "</size>" + "\n" + "<size=%30>" + "https://discord.gg/3Apx3JTr2Q</color></size>";
				data.AllowedToStartGame = true;// 
			}
		}

		[HarmonyPatch(typeof(CP_Bioscan_Core), "Master_OnPlayerScanChangedCheckProgress")]
		public class Patch_CP_Bioscan_Core
		{
			public static void Prefix(ref float scanProgress)
			{
				if (allowFastScan)
				{
					scanProgress = scanProgress + 100f; ;
				}
			}
		}


		//		[HarmonyPatch(typeof(PlayerManager), "OnPlayerSpawned", new Type[]
		//{
		//		typeof(pPlayerSpawnData),
		//		typeof(PlayerReplicator)
		//})]
		//		public class PlayerManager_OnPlayerSpawned
		//		{
		//			[HarmonyPrefix]
		//			public static void Prefix(ref pPlayerSpawnData spawnData, ref PlayerReplicator replicator)
		//			{
		//				bool flag = replicator.OwningPlayer.IsLocal && namespoof != "";
		//				if (flag)
		//				{
		//					replicator.OwningPlayer.NickName = namespoof;
		//				}
		//			}
		//		}


		[HarmonyPatch(typeof(PlayerManager), "OnPlayerSpawned", new Type[]
{
		typeof(pPlayerSpawnData),
		typeof(PlayerReplicator),
})]
		public class PlayerManager_OnPlayerSpawned
		{
			[HarmonyPrefix]
			public static void Prefix(PlayerManager __instance, ref pPlayerSpawnData spawnData, ref PlayerReplicator replicator)
			{
				bool flag = replicator.OwningPlayer.IsLocal && namespoof != "";
				if (flag)
				{
					replicator.OwningPlayer.NickName = namespoof;
				}
				PlayerAgent component = replicator.ReplicatorSupplier.gameObject.GetComponent<PlayerAgent>();
				component.gameObject.transform.localScale = (component.gameObject.transform.localScale + new Vector3(psizemulf, psizemulf, psizemulf));
			}
		}




		[HarmonyPatch(typeof(SNet_SyncManager), "OnPlayerSpawnedAgent", new Type[]
{
		typeof(SNet_Player)
})]
		public class SNet_SyncManager_OnPlayerSpawnedAgent
		{
			[HarmonyPrefix]
			public static void Prefix(ref SNet_Player player)
			{
				bool flag = player.IsLocal && namespoof != "";
				if (flag)
				{
					player.NickName = namespoof;
				}
			}
		}

		//[HarmonyPatch(typeof(CM_PageLoadout), "UpdateReadyButton")]
		//public class CM_PageLoadout_UpdateReadyButton
		//{
		//	[HarmonyPostfix]
		//	public static void Postfix(CM_PageLoadout __instance)
		//	{
		//		MainMenuGuiLayer.Current.PageLoadout.m_readyButton.SetText("Lets Go Kill Shit!!!!!");
		//	}
		//}

		[HarmonyPatch(typeof(CM_ExpeditionWindow), "Setup", new Type[]
{
		typeof(CM_PageRundown_New)
})]
		public class CM_ExpeditionWindow_setup
		{
			[HarmonyPostfix]
			public static void Postfix(CM_ExpeditionWindow __instance, ref CM_PageRundown_New basePage)
			{
				__instance.m_hostButton.SetText("HOST WITH GTFOMENU");
			}
		}



		[HarmonyPatch(typeof(CM_PageIntro), "Setup", new Type[]
{
		typeof(MainMenuGuiLayer)
})]
		public class CM_PageIntro_setup
		{
			[HarmonyPostfix]
			public static void Postfix(CM_PageIntro __instance, ref MainMenuGuiLayer guiLayer)
			{
				__instance.m_buttonStart.SetText("MENU LOADED. LETS GO");
			}
		}

		#endregion

		[HarmonyPostfix]
		public static void Start()
		{
			setupslider();
			RecoilDataBlocks();
			ArchetypeDataBlocksCustomized();
			GetAllPlayerOfflineGearDataBlocksEnabled();
			GetRundownBlocksCustomized();
			//exptest();
			MelonLogger.Log("[Trainer] I'm Starting Up...");
			GetAllDatablocksPrinted();
			LayerManager.MASK_BULLETWEAPON_RAY = LayerMask.GetMask(new string[]
			{
				"Default",
				"Default_NoGraph",
				"Default_BlockGraph",
				"EnemyDamagable",
				"ProjectileBlocker",
				"Dynamic"
			});

			_mainWindow = new Rect(20f, 60f, 250f, 150f);
			_playerVisualWindow = new Rect(20f, 220f, 250f, 150f);
			_miscVisualWindow = new Rect(20f, 260f, 250f, 150f);
			_aimbotVisualWindow = new Rect(20f, 260f, 250f, 150f);
		}

		[HarmonyPostfix]
		public void OnUpdate()
		{
			if (SteamManager.Initialized)
			{
				SteamManager.LocalPlayerID = 1337;
				if (namespoof != "")
				SteamManager.LocalPlayerName = namespoof;
			}

			if (SNet.Slots.GetPlayerInSlot(0) != null)
			{
				GuiManager.PlayerLayer.m_wardenObjective.m_pressBtn_Text.SetText("Host : " + SNet.Slots.GetPlayerInSlot(0).NickName + "\n" + "<size=%30>Hotkeys:<color=#e8ad09></color>" + "\n" + "[F2]-Toggle Freecam" + "\n" + "[F3]-TP Enemy to crosshair" + "\n" + "[F4]-Kill player in crosshair" + "\n" + "[F5]-Open on crosshair" + "\n" + "[F6]-Kill Enemy in crosshairs" + "\n" + "[F7]-Kill Nearby" + "\n" + "[F8]-Complete Level" + "</size>");
			}

			SNet.Core.SetFriendsData(FriendsDataType.Status, "<size=%60><color=#e8ad09>GTFOMENU</color>");
			SNet.Core.SetFriendsData(FriendsDataType.ExpeditionName, "<size=%60><color=#e8ad09>GTFOMENU</color>");
			enablestuff();
			GetWardenObjectiveInfo();
			PlayerAgent localPlayerAgent = PlayerManager.GetLocalPlayerAgent();
			if (localPlayerAgent != null)
			{

				if (texttoequip != "")
				{
					givewep();
				}

				if (bgodmode && localPlayerAgent.NeedHealth())
                {
					healmedamnit();
                }
				updatemembers();
				UpdateZone(localPlayerAgent);
				UpdateStats();
				UpdateSmartSneaking(localPlayerAgent);
				Updateintensity(localPlayerAgent);
				CheckInputs(localPlayerAgent);
				OnMeleeHitDamage();
				resetsentry();
			}
			else
			{
				pregamemenu();
			}
		}

		public static void setupslider()
		{
			_whitePixel = new Texture2D(1, 1, TextureFormat.ARGB32, false);
			_whitePixel.SetPixel(0, 0, Color.white);
			_whitePixel.Apply();

			_blackPixel = new Texture2D(1, 1, TextureFormat.ARGB32, false);
			_blackPixel.SetPixel(0, 0, Color.black);
			_blackPixel.Apply();

			_sliderBackgroundStyle = new GUIStyle();
			_sliderBackgroundStyle.padding = new RectOffset(2, 2, 2, 2);
			_sliderBackgroundStyle.normal.background = _whitePixel;
			_sliderBackgroundStyle.hover.background = _whitePixel;
			_sliderBackgroundStyle.active.background = _whitePixel;
			_sliderBackgroundStyle.focused.background = _whitePixel;

			_sliderThumbStyle = new GUIStyle();
			_sliderThumbStyle.stretchHeight = true;
			_sliderThumbStyle.fixedWidth = 20f;
			_sliderThumbStyle.normal.background = _blackPixel;
			_sliderThumbStyle.hover.background = _blackPixel;
			_sliderThumbStyle.active.background = _blackPixel;
			_sliderThumbStyle.focused.background = _blackPixel;
		}

			public static void pregamemenu()
		{
			if (Input.GetKeyDown(KeyCode.Delete))
			{
				pregame = !pregame;
				ShowCursor = !ShowCursor;
			}
			if (Input.GetKeyDown(KeyCode.F9))
			{
				Application.OpenURL("http://discord.gg/3Apx3JTr2Q");
			}
		}

		public static void poo()
        {

			if (openmenu)
			{

				if(tab1)
                {
					tab2 = false;
					tab3 = false;
					tab4 = false;
					tab5 = false;
				}
				if (tab2)
				{
					tab1 = false;
					tab3 = false;
					tab4 = false;
					tab5 = false;
				}
				if (tab3)
				{
					tab2 = false;
					tab1 = false;
					tab4 = false;
					tab5 = false;
				}
				if (tab4)
				{
					tab2 = false;
					tab3 = false;
					tab1 = false;
					tab5 = false;
				}
				if (tab5)
				{
					tab2 = false;
					tab3 = false;
					tab4 = false;
					tab1 = false;
				}
				pregame = false;
				godbuttonstyle = new GUIStyle(GUI.skin.GetStyle("button"));
				buttonstyle = new GUIStyle(GUI.skin.GetStyle("button"));
				buttonstyle.fontSize = 18;
				float thex = 10;
				//				Tab = GUI.Toggle(new Rect(thex, 10, Screen.width / 5, 40), Tab == 1, "Player", GUI.skin.button) ? 1 : Tab;
				tab1 = GUI.Toggle(new Rect(thex, 10, Screen.width / 5, 40), tab1, "Player", buttonstyle);
				thex += Screen.width / 5;
				tab2 = GUI.Toggle(new Rect(thex, 10, Screen.width / 5, 40), tab2, "Enemy", buttonstyle);
				thex += Screen.width / 5;
				tab3 = GUI.Toggle(new Rect(thex, 10, Screen.width / 5, 40), tab3, "Other", buttonstyle);
				thex += Screen.width / 5;
				tab4 = GUI.Toggle(new Rect(thex, 10, Screen.width / 5, 40), tab4, "Equip", buttonstyle);
				thex += Screen.width / 5;
				tab5 = GUI.Toggle(new Rect(thex, 10, Screen.width / 5, 40), tab5, "Kick Menu", buttonstyle);
				//thex += Screen.width / 5;
				//tab5 = GUI.Toggle(new Rect(thex, 10, Screen.width / 5, 40), tab5, "CLOSE", buttonstyle);
				float cY = 40f;
				if (tab1)
				{
					cY += 10;
					bgodmode = GUI.Toggle(new Rect(thex - (Screen.width / 5) - (Screen.width / 5) - (Screen.width / 5) - (Screen.width / 5), cY, Screen.width / 5, 40), bgodmode, "God Mode: " + (bgodmode ? "ON" : "OFF"), buttonstyle);
					cY += 40;
					smartsneak = GUI.Toggle(new Rect(thex - (Screen.width / 5) - (Screen.width / 5) - (Screen.width / 5) - (Screen.width / 5), cY, Screen.width / 5, 40), smartsneak, "Smart Sneak: " + (smartsneak ? "ON" : "OFF"), buttonstyle);
					cY += 40;
					bammo = GUI.Toggle(new Rect(thex - (Screen.width / 5) - (Screen.width / 5) - (Screen.width / 5) - (Screen.width / 5), cY, Screen.width / 5, 40), bammo, "Inf Ammo: " + (bammo ? "ON" : "OFF"), buttonstyle);
					cY += 40;
					if (GUI.Button(new Rect(thex - (Screen.width / 5) - (Screen.width / 5) - (Screen.width / 5) - (Screen.width / 5), cY, Screen.width / 5, 40), "Give team health", buttonstyle)) healteam();
					cY += 40;
					if (GUI.Button(new Rect(thex - (Screen.width / 5) - (Screen.width / 5) - (Screen.width / 5) - (Screen.width / 5), cY, Screen.width / 5, 40), "High Jump:  " + (bjump ? "ON" : "OFF"), buttonstyle)) bjump = !bjump;
					cY += 40;
					if (GUI.Button(new Rect(thex - (Screen.width / 5) - (Screen.width / 5) - (Screen.width / 5) - (Screen.width / 5), cY, Screen.width / 5, 40), "Players Shit Foam", buttonstyle)) shitfoam();
					cY += 40;
					if (GUI.Button(new Rect(thex - (Screen.width / 5) - (Screen.width / 5) - (Screen.width / 5) - (Screen.width / 5), cY, Screen.width / 5, 40), "Revive self", buttonstyle)) reviveself();
					cY += 40;
					if (GUI.Button(new Rect(thex - (Screen.width / 5) - (Screen.width / 5) - (Screen.width / 5) - (Screen.width / 5), cY, Screen.width / 5, 40), "Fast run:  " + (brun ? "ON" : "OFF"), buttonstyle)) brun = !brun;
					cY += 40;

				}
				if (tab2)
				{
					cY += 10;
					esp = GUI.Toggle(new Rect(thex - (Screen.width / 5) - (Screen.width / 5) - (Screen.width / 5), cY, Screen.width / 5, 40), esp, "ESP: " + (esp ? "ON" : "OFF"), buttonstyle);
					cY += 40;
					if (GUI.Button(new Rect(thex - (Screen.width / 5) - (Screen.width / 5) - (Screen.width / 5), cY, Screen.width / 5, 40), "MMB spawn: " + (bspawn ? "ON " : "OFF"), buttonstyle)) bspawn = !bspawn;
					cY += 40;
					if (GUI.Button(new Rect(thex - (Screen.width / 5) - (Screen.width / 5) - (Screen.width / 5), cY, Screen.width / 5, 40), "Foam All", buttonstyle)) foamall();
					cY += 40;
					if (GUI.Button(new Rect(thex - (Screen.width / 5) - (Screen.width / 5) - (Screen.width / 5), cY, Screen.width / 5, 40), "Detection: " + (Global.EnemyPlayerDetectionEnabled ? "ON " : "OFF"), buttonstyle)) Global.EnemyPlayerDetectionEnabled = !Global.EnemyPlayerDetectionEnabled;
					cY += 40;
					if (GUI.Button(new Rect(thex - (Screen.width / 5) - (Screen.width / 5) - (Screen.width / 5), cY, Screen.width / 5, 40), "Spawn Survival", buttonstyle)) spawnsurvival();
					cY += 40;
				}
				if (tab3)
				{
					cY += 10;
					allowFastScan = GUI.Toggle(new Rect(thex - (Screen.width / 5) - (Screen.width / 5), cY, Screen.width / 5, 40), allowFastScan, "Instant bioscan: " + (allowFastScan ? "ON" : "OFF"), buttonstyle);
					cY += 40;
					if (GUI.Button(new Rect(thex - (Screen.width / 5) - (Screen.width / 5), cY, Screen.width / 5, 40), "Toggle Fog", buttonstyle)) togglefog();
					cY += 40;
					if (GUI.Button(new Rect(thex - (Screen.width / 5) - (Screen.width / 5), cY, Screen.width / 5, 40), "Open all doors", buttonstyle)) openalldoors();
					cY += 40;
					if (GUI.Button(new Rect(thex - (Screen.width / 5) - (Screen.width / 5), cY, Screen.width / 5, 40), "Remove Locks", buttonstyle)) removelocks();
					cY += 40;
					if (GUI.Button(new Rect(thex - (Screen.width / 5) - (Screen.width / 5), cY, Screen.width / 5, 40), "Hack all", buttonstyle)) hackall();
					cY += 40;
					if (GUI.Button(new Rect(thex - (Screen.width / 5) - (Screen.width / 5), cY, Screen.width / 5, 40), "Pickup all items", buttonstyle)) pickupallitems();
					cY += 40;
					if (GUI.Button(new Rect(thex - (Screen.width / 5) - (Screen.width / 5), cY, Screen.width / 5, 40), "Restart Level", buttonstyle)) restatlevel();	
					cY += 40;
					if (GUI.Button(new Rect(thex - (Screen.width / 5) - (Screen.width / 5), cY, Screen.width / 5, 40), "Freeze Players", buttonstyle)) freeze();
					cY += 40;
				}
				if (tab4)
				{
					float thexa = thex - (Screen.width / 5) - (Screen.width / 5) - (Screen.width / 5);
					cY += 40;
					texttoequip = GUI.Toggle(new Rect(thexa, cY, Screen.width / 5, 40), texttoequip == "S870", "S870", buttonstyle) ? "S870" : texttoequip;
					cY += 40;
					texttoequip = GUI.Toggle(new Rect(thexa, cY, Screen.width / 5, 40), texttoequip == "TechMan Veruta XII", "TechMan Veruta XII", buttonstyle) ? "TechMan Veruta XII" : texttoequip;
					cY += 40;
					texttoequip = GUI.Toggle(new Rect(thexa, cY, Screen.width / 5, 40), texttoequip == "Shelling S49", "Shelling S49", buttonstyle) ? "Shelling S49" : texttoequip;
					cY += 40;
					texttoequip = GUI.Toggle(new Rect(thexa, cY, Screen.width / 5, 40), texttoequip == "Bataldo Model 8", "Bataldo Model 8", buttonstyle) ? "Bataldo Model 8" : texttoequip;
					cY += 40;
					texttoequip = GUI.Toggle(new Rect(thexa, cY, Screen.width / 5, 40), texttoequip == "Raptus Steigro", "Raptus Steigro", buttonstyle) ? "Raptus Steigro" : texttoequip;
					cY += 40;
					texttoequip = GUI.Toggle(new Rect(thexa, cY, Screen.width / 5, 40), texttoequip == "Malatack LX", "Malatack LX", buttonstyle) ? "Malatack LX" : texttoequip;
					cY += 40;
					texttoequip = GUI.Toggle(new Rect(thexa, cY, Screen.width / 5, 40), texttoequip == "LTC5", "LTC5", buttonstyle) ? "LTC5" : texttoequip;
					cY += 40;
					texttoequip = GUI.Toggle(new Rect(thexa, cY, Screen.width / 5, 40), texttoequip == "CAB", "CAB", buttonstyle) ? "CAB" : texttoequip;
					cY = 80;
					float thexb = thex - (Screen.width / 5) - (Screen.width / 5);
					texttoequip = GUI.Toggle(new Rect(thexb, cY, Screen.width / 5, 40), texttoequip == "TR22", "TR22", buttonstyle) ? "TR22" : texttoequip;
					cY += 40;
					texttoequip = GUI.Toggle(new Rect(thexb, cY, Screen.width / 5, 40), texttoequip == "Malatack HXC", "Malatack HXC", buttonstyle) ? "Malatack HXC" : texttoequip;
					cY += 40;
					texttoequip = GUI.Toggle(new Rect(thexb, cY, Screen.width / 5, 40), texttoequip == "Buckland AF6", "Buckland AF6", buttonstyle) ? "Buckland AF6" : texttoequip;
					cY += 40;
					texttoequip = GUI.Toggle(new Rect(thexb, cY, Screen.width / 5, 40), texttoequip == "Buckland XDist2", "Buckland XDist2", buttonstyle) ? "Buckland XDist2" : texttoequip;
					cY += 40;
					texttoequip = GUI.Toggle(new Rect(thexb, cY, Screen.width / 5, 40), texttoequip == "Mastaba R66", "Mastaba R66", buttonstyle) ? "Mastaba R66" : texttoequip;
					cY += 40;
					texttoequip = GUI.Toggle(new Rect(thexb, cY, Screen.width / 5, 40), texttoequip == "Maul", "Maul", buttonstyle) ? "Maul" : texttoequip;
					cY += 40;
					texttoequip = GUI.Toggle(new Rect(thexb, cY, Screen.width / 5, 40), texttoequip == "Mallet", "Mallet", buttonstyle) ? "Mallet" : texttoequip;
					cY += 40;
					texttoequip = GUI.Toggle(new Rect(thexb, cY, Screen.width / 5, 40), texttoequip == "TechMan Klust 6", "TechMan Klust 6", buttonstyle) ? "TechMan Klust 6" : texttoequip;
					cY = 80;
					float thexc = thex - (Screen.width / 5);
					texttoequip = GUI.Toggle(new Rect(thexc, cY, Screen.width / 5, 40), texttoequip == "Köning PR 11", "Köning PR 11", buttonstyle) ? "Köning PR 11" : texttoequip;
					cY += 40;
					texttoequip = GUI.Toggle(new Rect(thexc, cY, Screen.width / 5, 40), texttoequip == "Sledgehammer", "Sledgehammer", buttonstyle) ? "Sledgehammer" : texttoequip;
					cY += 40;
					texttoequip = GUI.Toggle(new Rect(thexc, cY, Screen.width / 5, 40), texttoequip == "Stalwart Flow G2", "Stalwart Flow G2", buttonstyle) ? "Stalwart Flow G2" : texttoequip;
					cY += 40;
					texttoequip = GUI.Toggle(new Rect(thexc, cY, Screen.width / 5, 40), texttoequip == "D-tek Optron IV", "D-tek Optron IV", buttonstyle) ? "D-tek Optron IV" : texttoequip;
					cY += 40;
					texttoequip = GUI.Toggle(new Rect(thexc, cY, Screen.width / 5, 40), texttoequip == "Krieger O4", "Krieger O4", buttonstyle) ? "Krieger O4" : texttoequip;
					cY += 40;
					texttoequip = GUI.Toggle(new Rect(thexc, cY, Screen.width / 5, 40), texttoequip == "RAD Labs Meduza", "RAD Labs Meduza", buttonstyle) ? "RAD Labs Meduza" : texttoequip;
					cY += 40;
					texttoequip = GUI.Toggle(new Rect(thexc, cY, Screen.width / 5, 40), texttoequip == "Autotek 51 RSG", "Autotek 51 RSG", buttonstyle) ? "Autotek 51 RSG" : texttoequip;
					cY += 40;
					texttoequip = GUI.Toggle(new Rect(thexc, cY, Screen.width / 5, 40), texttoequip == "Mechatronic SGB3", "Mechatronic SGB3", buttonstyle) ? "Mechatronic SGB3" : texttoequip;
					cY += 40;
				}
				if (tab5)
				{
					cY += 10;
					if (SNet.Slots.GetPlayerInSlot(0).NickName != null)
					{
						if (GUI.Button(new Rect(thex, cY, Screen.width / 5, 40), "Kick: " + SNet.Slots.GetPlayerInSlot(0).NickName, buttonstyle)) kickuser(SNet.Slots.GetPlayerInSlot(0));
					}
					if (SNet.Slots.GetPlayerInSlot(1).NickName != null)
					{
						cY += 40;
						if (GUI.Button(new Rect(thex, cY, Screen.width / 5, 40), "Kick: " + SNet.Slots.GetPlayerInSlot(1).NickName, buttonstyle)) kickuser(SNet.Slots.GetPlayerInSlot(1));
					}
					if (SNet.Slots.GetPlayerInSlot(2).NickName != null)
					{
						cY += 40;
						if (GUI.Button(new Rect(thex, cY, Screen.width / 5, 40), "Kick: " + SNet.Slots.GetPlayerInSlot(2).NickName, buttonstyle)) kickuser(SNet.Slots.GetPlayerInSlot(2));
					}
					if (SNet.Slots.GetPlayerInSlot(3).NickName != null)
					{					
						cY += 40;
						if (GUI.Button(new Rect(thex, cY, Screen.width / 5, 40), "Kick: " + SNet.Slots.GetPlayerInSlot(3).NickName, buttonstyle)) kickuser(SNet.Slots.GetPlayerInSlot(3));
					}
				}
			}
        }

		public static void openthing()
		{
			PlayerAgent localPlayerAgent = PlayerManager.GetLocalPlayerAgent();
			iLG_Door_Core iLG_Door_Core = localPlayerAgent.FPSCamera.CameraRayObject.GetComponentInParent<iLG_Door_Core>();
			if (iLG_Door_Core != null)
			{
				iLG_Door_Core.AttemptOpenCloseInteraction();
				if (iLG_Door_Core.LastStatus != eDoorStatus.Open)
				{
					iLG_Door_Core.AttemptDamage(eDoorDamageType.EnemyHeavy, new Vector3(0, 0, 0));
				}
			}
		}
		public enum submenuz : int
		{	
			Main, //this is the place holder, 
			Playerz,
			Enemyz,
			Otherz,
			Equipz
		}


		public static void changeSubmenu(int newSubmenu)
		{
			smenu = newSubmenu;
		}

		public static void addOption(string option, bool res)
		{
			oCount++;
			Lastmenu[oCount] = GUI.Toggle(new Rect(10, cY, 250, 40), res, $"{option}: {(res ? "On" : "Off")}", GUI.skin.button);
			cY += 40;
		}

		public static void addSubMenuOption(string option, int newSubmenu) //Shouldn't do that tho, we aren't adding the submenu options to that array. wtf no idea
		{
			//Options.Player.GodMode = GUI.Toggle(new Rect(offsetX + 150f, cY, 20f, 30f), Options.Player.GodMode, ""); ----

			bool thing = GUI.Toggle(new Rect(thex, 10, Screen.width / 4, 40), optionPress, option, GUI.skin.button); // they are 2 different. look at the toggle. above
			thex += Screen.width / 4;
			if (thing)
			{
				MelonLogger.Log("Boop");
				optionPress = true;
				changeSubmenu(newSubmenu);
			}
			optionPress = false;
		}

		public static void ResetVars() //this will force everything to it's original state, because we are also calling the 'addoption' funciton in the switch/case, it is basically re-writing every option on each frame if that makes sense. yes
        {
			thex = 10.0f;
			cY = 40;
			optionPress = false;
            for (int i = 0; i < Lastmenu.Length; i++)
                Lastmenu[i] = false;
        }

		[HarmonyPostfix]
		public void OnGUI()
		{
			Rect ff;
			Rect mainRect;
			GUIStyle guistyle = new GUIStyle(GUI.skin.GetStyle("label"));
			guistyle.fontSize = 0x14;

			GUIStyle fontSize = new GUIStyle(GUI.skin.GetStyle("label"));
			fontSize.fontSize = 14;
			//fontSize.normal.textColor = clr;
			Rect windowRect = new Rect((float)100f, 70f, 270, 200);

			if (pregame)
			{
				GUI.Label(new Rect(10, 15, 100, 50), "<Color=yellow>" + "NameSpoof: " + "</Color>");
				namespoof = GUI.TextField(new Rect(90, 15, 200, 25), namespoof, 50);
				GUI.Label(new Rect(10, 65, 100, 80), "<Color=yellow>" + "Enemy Size: " + "\n" + (sizemulf + 1f).ToString() + "</Color>");
				sizemulf = GUI.HorizontalSlider(new Rect(90, 65, 200, 25), sizemulf, 0.0F, 4.0F, _sliderBackgroundStyle, _sliderThumbStyle);
				GUI.Label(new Rect(10, 100, 100, 80), "<Color=yellow>" + "Player Size: " + "\n" + (psizemulf + 1f).ToString() + "</Color>");
				psizemulf = GUI.HorizontalSlider(new Rect(90, 100, 200, 25), psizemulf, 0.0F, 4.0F, _sliderBackgroundStyle, _sliderThumbStyle);

			}

			poo();
			int windowWidth = 450;
			int windowHeight = 60;
			int screenbottom = Screen.height;
			int screenmiddlewidth = (int)(Screen.width * 0.5) - (int)(windowWidth * 0.5);
			//int x = (Screen.width - windowWidth) / 2;
			//int y = (Screen.height - windowWidth) / 2;
			//int xpos = (Screen.width - windowWidth);
			//int ypos = (Screen.height - windowHeight);
			int xpos = ((Screen.width / 2 - windowWidth));
			int ypos = (Screen.height / 2 - windowHeight);
			Rect Menu11 = new Rect(xpos, ypos, windowWidth, windowHeight);
			Rect Menu12 = new Rect(xpos, ypos + 50, windowWidth, windowHeight);
			Rect Menu13 = new Rect(xpos, ypos + 100, windowWidth, windowHeight);
			Rect Menu14 = new Rect(xpos, ypos + 300, windowWidth, windowHeight);
			Rect Menu15 = new Rect(xpos, ypos, windowWidth, windowHeight);

			//t++;
			//if (t % 49 < 0)
			//	clr = Color.gray;
			//if (t % 100 < 50)
			//	clr = Color.white;
			//if (t % 200 < 101)
			//	clr = Color.red;
			//if (t % 300 < 201)
			//	clr = Color.blue;
			//if (t % 400 < 301)
			//	clr = Color.green;
			//if (t % 500 < 401)
			//	clr = Color.yellow;
			if (esp)
			{
				ESP();
			}
			if (FocusStateManager.CurrentState == eFocusState.FPS)
			{
				if (MyZone != "NONE")
				{
					guistyle.fontSize = 0x14;
					GUI.Label(new Rect(Screen.width / 2 - (windowWidth / 2), Screen.height - (windowHeight / 2), windowWidth, windowHeight), MyZone + " [Enemies Remaining: " + "<Color=Green>" + themembers.Count.ToString() + "</Color>]", guistyle);
					//GUI.Label(Menu15, MyZone, guistyle);
				}
			}
		}


		public static void PlaceMineAtCrosshair()
		{
			ItemReplicationManager.SpawnItem(new pItemData
			{
				itemID_gearCRC = 0x7DU
			}, null, ItemMode.Instance, PlayerManager.GetLocalPlayerAgent().FPSCamera.CameraRayPos, Quaternion.LookRotation(PlayerManager.GetLocalPlayerAgent().FPSCamera.CameraRayNormal * -1f, PlayerManager.GetLocalPlayerAgent().transform.forward), PlayerManager.GetLocalPlayerAgent().CourseNode, PlayerManager.GetLocalPlayerAgent());
		}

		public static class EnumUtil
		{
			public static IEnumerable<T> GetValues<T>()
			{
				return (IEnumerable<T>)Enum.GetValues(typeof(T)).Cast<T>();
			}
		}


		public static void enablestuff()
        {
			GearCategory.internalEnabled = true;
			Archetype.internalEnabled = true;
			PlayerOffline.internalEnabled = true;
			EnemyData.internalEnabled = true;
			RundownDatblk.internalEnabled = true;
			//Il2CppArrayBase<GearCategoryDataBlock> gearblocks = GameDataBlockBase<GearCategoryDataBlock>.GetAllBlocks();
			//for (int i = 0; i < gearblocks.Count; i++)
			//{
			//	gearblocks[i].internalEnabled = true;
			//}
			//Il2CppArrayBase<ArchetypeDataBlock> ArchetypeDataBlock = GameDataBlockBase<ArchetypeDataBlock>.GetAllBlocks();
			//for (int i = 0; i < ArchetypeDataBlock.Count; i++)
			//{
			//	ArchetypeDataBlock[i].internalEnabled = true;
			//}
			//for (int i = 0; i < GameDataBlockBase<PlayerOfflineGearDataBlock>.Wrapper.Blocks.Count; i++)
			//{
			//	PlayerOfflineGearDataBlock playerOfflineGearDataBlock = GameDataBlockBase<PlayerOfflineGearDataBlock>.Wrapper.Blocks[i];
			//	if (playerOfflineGearDataBlock != null)
			//	{
			//		playerOfflineGearDataBlock.internalEnabled = true;
			//	}
			//}
			//Il2CppArrayBase<EnemyDataBlock> enemiesenambled = GameDataBlockBase<EnemyDataBlock>.GetAllBlocks();
			//for (int i = 0; i < enemiesenambled.Count; i++)
			//{
			//	enemiesenambled[i].internalEnabled = true;
			//}

		}

		public static void givewep()
        {

			//Il2CppArrayBase<GearCategoryDataBlock> gearblocks = GameDataBlockBase<GearCategoryDataBlock>.GetAllBlocks();
			PlayerAgent Snetagent = SNet.Master.PlayerAgent.TryCast<PlayerAgent>();
            PlayerAgent localPlayerAgent = PlayerManager.GetLocalPlayerAgent();
            PlayerInventoryLocal playerInventoryLocal = localPlayerAgent.Inventory.Cast<PlayerInventoryLocal>();
            PlayerBackpack backpack = PlayerBackpackManager.GetBackpack(localPlayerAgent.Owner);
            PlayerAmmoStorage ammoStorage = backpack.AmmoStorage;
                foreach (InventorySlot foo in Enum.GetValues(typeof(InventorySlot)))
                {
                    GearIDRange[] allGearForSlot = GearManager.GetAllGearForSlot(foo);
				foreach (GearIDRange idr in allGearForSlot)
				{
					if (texttoequip != "")
					{
						if (idr.PublicGearName.ToLower().Contains(texttoequip.ToLower()))
						{
							AmmoType thetype = PlayerAmmoStorage.GetAmmoTypeFromSlot(foo);
							texttoequip = "";
							MelonLogger.Log("Trying: " + idr.PublicGearName);
                            //PlayerBackpackManager.ResetLocalAmmoStorage(false);
                            //MelonLogger.Log(idr.PlayfabItemId);
                            //MelonLogger.Log(block.persistentID);
                            //block.internalEnabled = true;
                            PlayerBackpackManager.ResetLocalAmmoStorage(true);
							PlayerBackpackManager.EquipLocalGear(idr);
							GearManager.RegisterGearInSlotAsEquipped(idr.PlayfabItemId, foo);
							playerInventoryLocal.WieldedItem.SetCurrentClipRel(playerInventoryLocal.WieldedItem.GetMaxClip());
							playerInventoryLocal.WieldedItem.SetCurrentClip(playerInventoryLocal.WieldedItem.GetMaxClip());
							if (thetype == AmmoType.Special)
							{
								PlayerBackpackManager.GiveAmmoToPlayer(SNet.Slots.GetPlayerInSlot(0), 0, backpack.AmmoStorage.GetBulletMaxCap(PlayerAmmoStorage.GetAmmoTypeFromSlot(foo)), 0);
								backpack.AmmoStorage.SpecialAmmo.AddAmmo(backpack.AmmoStorage.GetBulletMaxCap(AmmoType.Special));
							}
							if (thetype == AmmoType.Standard)
							{
								PlayerBackpackManager.GiveAmmoToPlayer(SNet.Slots.GetPlayerInSlot(0), backpack.AmmoStorage.GetBulletMaxCap(PlayerAmmoStorage.GetAmmoTypeFromSlot(foo)), 0, 0);
								backpack.AmmoStorage.StandardAmmo.AddAmmo(backpack.AmmoStorage.GetBulletMaxCap(AmmoType.Standard));
							}
							if (thetype == AmmoType.Class)
							{
								PlayerBackpackManager.GiveAmmoToPlayer(SNet.Slots.GetPlayerInSlot(0), 0, 0, backpack.AmmoStorage.GetBulletMaxCap(PlayerAmmoStorage.GetAmmoTypeFromSlot(foo)));
								backpack.AmmoStorage.ClassAmmo.AddAmmo(backpack.AmmoStorage.GetBulletMaxCap(AmmoType.Class));
							}
							PlayerBackpackManager.LocalBackpack.AmmoStorage.PickupAmmo(PlayerAmmoStorage.GetAmmoTypeFromSlot(foo), 1000);
							backpack.AmmoStorage.PickupAmmo(PlayerAmmoStorage.GetAmmoTypeFromSlot(foo), backpack.AmmoStorage.GetBulletMaxCap(PlayerAmmoStorage.GetAmmoTypeFromSlot(foo)));
							backpack.AmmoStorage.SetAmmo(PlayerAmmoStorage.GetAmmoTypeFromSlot(foo), backpack.AmmoStorage.GetBulletMaxCap(PlayerAmmoStorage.GetAmmoTypeFromSlot(foo)));
							MelonLogger.Log("did i equip?");
							break;
						}
					}

				}
            }
        }


        public static void NoClip()
		{
			PlayerAgent localPlayerAgent = PlayerManager.GetLocalPlayerAgent();
			RaycastHit raycastHit;
			bool flag = Physics.Raycast(localPlayerAgent.CamPos, localPlayerAgent.FPSCamera.Forward, out raycastHit, 100f);
			if (flag)
			{
				raycastHit.transform.gameObject.SetActive(false);
			}
		}

	

        public static void ESP()
		{
			Rect rect;
			string state = "";
			Color thecolor = Color.blue;
			PlayerAgent localPlayerAgent = PlayerManager.GetLocalPlayerAgent();
			Il2CppSystem.Collections.Generic.List<EnemyAgent> reachableEnemiesInNodes = AIG_CourseGraph.GetReachableEnemiesInNodes(localPlayerAgent.CourseNode, 20);
			if (reachableEnemiesInNodes != null || reachableEnemiesInNodes.Count > 0)
			{
				foreach (EnemyAgent player in reachableEnemiesInNodes)
				{
					if (player != null)
					{
						UnityEngine.Object enemyobject = player;
						Camera cam = CameraManager.GetCurrentCamera();
						Vector3 vector = player.transform.position;
						Vector3 vector2 = vector;
						vector2.y += 1.8f;
						vector = cam.WorldToScreenPoint(vector);
						vector2 = cam.WorldToScreenPoint(vector2);

						float eheadx = cam.WorldToScreenPoint(player.ModelRef.m_headBone.position).x;
						float eheady = cam.WorldToScreenPoint(player.ModelRef.m_headBone.position).y;


						//if (vector.z > 0f && vector2.z > 0f)
						if (vector.z > 0f)
						{
							Vector3 vector3 = GUIUtility.ScreenToGUIPoint(vector);
							vector3.y = (float)Screen.height - vector3.y;
							Vector3 vector4 = GUIUtility.ScreenToGUIPoint(vector2);
							vector4.y = (float)Screen.height - vector4.y;
							float num = Math.Abs(vector3.y - vector4.y) / 2.2f;
							float num2 = num / 2f;
							float heights = vector3.y - vector4.y;
							if (sizemulf > 0.0f)
                            {
								num = num *sizemulf;
								heights = heights * sizemulf;

							}
							rect = new Rect(vector4.x - num2, vector4.y, num, vector3.y - vector4.y);
							//rect = new Rect(vector.x - num2, vector.y, num, vector2.y - vector2.y);
							bool flag = player.IsHibernationDetecting && Clock.Time - player.HibernationDetectingStartedAt > 0.8f;
							//DrawRectangle(rect, thecolor);
							if (player.AI.Mode == AgentMode.Hibernate)
							{
								thecolor = Color.green;
								state = player.AI.Mode.ToString();
							}
							if (player.AI.Mode == AgentMode.Agressive)
							{
								thecolor = Color.red;
								state = player.AI.Mode.ToString();
							}
							if (player != null && player.Alive && (player.Position - player.ListenerPosition).sqrMagnitude < 80f && player.AI.Mode == AgentMode.Hibernate && flag && TrainerComponent.EnemyIsInSight(player))
							{
								thecolor = Color.yellow;
								state = "STOP MOVING!";
							}
							float enemyhealth = player.Damage.Health / player.Damage.HealthMax * 100;
							Vector3 position2 = localPlayerAgent.transform.position;
							Vector3 position3 = player.transform.position;
							Vector3 position4 = player.transform.position + new Vector3(0f, 2f, 0f);
							Vector3 vector7 = cam.WorldToScreenPoint(position4);
							if (vector7.z > 0f)
							{
								texture2 = new Texture2D(2, 2, TextureFormat.ARGB32, false);
								texture2.SetPixel(0, 0, thecolor);
								texture2.SetPixel(1, 0, thecolor);
								texture2.SetPixel(0, 1, thecolor);
								texture2.SetPixel(1, 1, thecolor);
								texture2.Apply();
								if ((int)Vector3.Distance(position2, position3) < 60)
								{
									DebugDraw3D.DrawLineCube(vector, heights, thecolor, 900f);
									DebugDraw3D.DrawLineCube(vector3, heights, thecolor, 900f);
									//Box(vector4.x, vector4.y + vector3.y - vector4.y, num, heights, texture2);
									DrawText(17, new Rect(vector7.x, (float)Screen.height - vector7.y, 200f, 20f), string.Concat(new object[]
											{
										"[" + enemyhealth.ToString() + "%]",
										" \n ",
										"[" + (int)Vector3.Distance(position2, position3) + " M]",
										"\n",
										"[" + (string)state + "]",
											}), thecolor, FontStyle.Normal);
									//DrawCircle(new Vector2(vector4.x, vector4.y + vector3.y - vector4.y), eheadx - eheady, Color.cyan);
								}
							}
						}
					}
				}

			}
		}


		public static void DrawText(int fontsize, Rect pos, string info, Color color, FontStyle fontstyle)
		{
			GUI.Label(pos, info, new GUIStyle
			{
				fontSize = fontsize,
				fontStyle = fontstyle,
				normal =
				{
					textColor = color
				}
			});
		}

		public static void DrawRectangle(Rect rect, Color color)
		{
			Vector3 v = new Vector3(rect.x, rect.y, 0f);
			Vector3 v2 = new Vector3(rect.x + rect.width, rect.y, 0f);
			Vector3 v3 = new Vector3(rect.x + rect.width, rect.y + rect.height, 0f);
			Vector3 v4 = new Vector3(rect.x, rect.y + rect.height, 0f);
			DrawLine(v, v2, color);
			DrawLine(v2, v3, color);
			DrawLine(v3, v4, color);
			DrawLine(v4, v, color);
		}

		public static void DrawLine(Vector2 start, Vector2 end, Color color)
		{
			Application.targetFrameRate = 100000;
			if (LineMat == null)
			{
				LineMat = new Material(Shader.Find("GUI/Text Shader"));
				LineMat.hideFlags = HideFlags.HideAndDontSave;
				LineMat.shader.hideFlags = HideFlags.HideAndDontSave;
				LineMat.SetInt("_SrcBlend", 3);
				LineMat.SetInt("_DstBlend", 3);
				LineMat.SetInt("_Cull", 0);
				LineMat.SetInt("_ZWrite", 0);
				LineMat.SetInt("_ZTest", 8);
			}
			LineMat.SetPass(0);
			GL.Begin(1);
			GL.Color(color);
			GL.Vertex3(start.x, start.y, 0f);
			GL.Vertex3(end.x, end.y, 0f);
			GL.End();
		}

		#region declarations


		public static float thex = 10.0f;
		public static float cY = 40f;
		public static int oCount;
		public static int cOption;
		public static int maxOptions = 18;
		public static int submenuLevel;
		public static int smenu;//its calling all cases no matter what Show the log?
		public static bool optionPress = false;
		static bool[] Lastmenu = new bool[10];


		public static bool displayLabel;
		public static bool pooz;
		public static bool tab1;
		public static bool tab2;
		public static bool tab3;
		public static bool tab4;
		public static bool tab5;
		public static string menuname;


		public static bool brun;
		public static float origspeed = -1;

		public static eRundownTier Tier;
		public static eRundownKey RnDownKey = eRundownKey.Local;
		public static uint RundownID;
		public static string Name;

		public static bool bspawn;
		public static GameDataBlockBase<GearCategoryDataBlock> GearCategory = new GameDataBlockBase<GearCategoryDataBlock>();
		public static GameDataBlockBase<ArchetypeDataBlock> Archetype = new GameDataBlockBase<ArchetypeDataBlock>();
		public static GameDataBlockBase<PlayerOfflineGearDataBlock> PlayerOffline = new GameDataBlockBase<PlayerOfflineGearDataBlock>();
		public static GameDataBlockBase<EnemyDataBlock> EnemyData = new GameDataBlockBase<EnemyDataBlock>();
		public static GameDataBlockBase<RundownDataBlock> RundownDatblk = new GameDataBlockBase<RundownDataBlock>();

		public static RundownList TheListofLevels;
		public static float orig1 = -1;
		public static float orig2 = -1;
		public static float orig3 = -1;

		public static bool breakdoor;
		public static bool bjump;
		public static Rect _mainWindow;
		public static Rect _playerVisualWindow;
		public static Rect _miscVisualWindow;
		public static Rect _aimbotVisualWindow;
		public static bool _visible = true;
		public static bool _playerEspVisualVisible;
		public static  bool _miscVisualVisible;
		public static bool _aimbotVisualVisible;

		public static float psizemulf = 0.0f;
		public static float sizemulf = 0.0F;
		public static string texttoequip = "";
		public static GUIStyle _sliderBackgroundStyle;
		public static GUIStyle _sliderThumbStyle;
		public static Texture2D _whitePixel;
		public static Texture2D _blackPixel;
		public static Slider sizemul;
		public static bool bammo;
		public static int t = 0;
		public static Color clr;
		public static bool isMageClass;
		public static bool pregame;
		public static string namespoof = "";
		public static bool smartsneak = false;
		public static bool bgodmode = false;
		public static Vector2 scrollPosition = Vector2.zero;
		public static Vector2 scrollPosition2 = Vector2.zero;
		public static float scroller;
		public static float scroller2;
		public static int Tab;
		public static GUIStyle godbuttonstyle;
		public static GUIStyle buttonstyleconst;
		public static GUIStyle buttonstyle;
		public static GUIStyle labelStyle;
		public static GUIStyle labelStylePos;
		public static int selectedPage = 0;
		public static string CurrentStateOverrideCodeMessage = "Reactor Code: {Code}";
		public static string CurrentStateOverrideCode = "";
		public static bool ShowCursor;
		public static eFocusState fsm;
		public static CM_Cursor cursor;
		public static CustomHudInfo[] teaminfo;
		public static CustomHudInfo customHudInfo;
		public static List<EnemyAgent> themembers = new List<EnemyAgent>();
		public static Rect MenuWindow = new Rect(10, 10, 200, 140);
		//public static PlayerAgent localPlayerAgent = PlayerManager.GetLocalPlayerAgent();
		//public static PlayerInventoryLocal playerInventoryLocal = localPlayerAgent.Inventory.TryCast<PlayerInventoryLocal>();
		//public static Il2CppSystem.Collections.Generic.List<EnemyAgent> reachableEnemiesInNodes = AIG_CourseGraph.GetReachableEnemiesInNodes(localPlayerAgent.CourseNode, 0xFFFF);
		//public static Camera cam = CameraManager.GetCurrentCamera();
		public static Texture2D _tex;
		public static Material LineMat;
		public static Dictionary<uint, GameObject> m_enemyTypeLookup = new Dictionary<uint, GameObject>();
		public static List<NavMarker> m_markersActive;
		public static Vector3 temppos;
		public static IEnumerable<PlayerAgent> _players;
		public static IEnumerable<EnemyHeartbeat> _enemies;
		private static Texture2D _coloredLineTexture;
		private static Color _coloredLineColor;
		public static bool esp;
		public static GUIStyle boxStyle = null;
		static public Texture2D texture2 = null;
		//public static PlayerInventoryLocal playerInventoryLocal = localPlayerAgent.Inventory.TryCast<PlayerInventoryLocal>();
		//public static MeleeWeaponFirstPerson meleeWeaponFirstPerson = playerInventoryLocal.WieldedItem.TryCast<MeleeWeaponFirstPerson>();
		//public static Il2CppSystem.Collections.Generic.List<EnemyAgent> reachableEnemiesInNodes = AIG_CourseGraph.GetReachableEnemiesInNodes(localPlayerAgent.CourseNode, 1000);
		//public static PlayerAgent localPlayerAgent = PlayerManager.GetLocalPlayerAgent();
		//Il2CppSystem.Collections.Generic.List<PlayerAgent> playersingame = PlayerManager.PlayerAgentsInLevel;
		public static List<RundownDownData> list;
		public static int CurrentExpInt;
		public static bool openmenu = false;
		public TrainerComponent trainercom;
		public static gtfoloader gtfoloader;
		public bool Reduction;
		public static bool allowFastScan;
		public bool FreeEnemiesMap;
		static CM_IntroTextField m_textLeft;
		public static System.Random random = new System.Random();
		static uint[] shitlist = new uint[] { 108, 109, 110, 74, 73, 100, 28, 97, 111, 37, 53, 125, 126, 27, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 146, 113, 30, 114, 140, 142, 136, 130, 115, 116, 117, 139, 144, 118, 119, 120, 121, 122, 101, 102, 127, 132, 128, 129, 147, 149, 150, 131, 133, 137, 141, 143, 145, 151, 138, 148 };
		static uint[] enemieslist = new uint[] { 7, 48, 42, 1, 8, 9, 12, 10, 11, 2, 6, 16, 17, 13, 18, 14, 15, 19, 40, 44, 41, 20, 21, 22, 50, 26, 27, 28, 45, 23, 43, 34, 24, 25, 29, 30, 36, 49, 31, 33, 32, 35, 46, 47, 37, 38 };
		public static bool Listed;
		public WatermarkGuiLayer m_custom_watermark;
		public static TrainerComponent tc;
		public static float m_next_intensity_update;
		public static bool BUNNY_HOP;
		public static float m_nextIdle_dialog_t;
		public static Vector2 m_idleDelay;
		public static bool m_dialog_enabled;
		public static float ORIGINAL_RUN_SPEED;
		public static PlayerAgent FollowPlayerAgent;
		public static float FASTEST_SPEED;
		public static EnemyAgent m_target;
		public static bool UpdateOnlyLocalDramaState;
		public static float m_lookatScoutTriggerTimer;
		public static float MeleePushTimer;
		public static bool PrintDataBlocks = true;
		public static uint Test_CustomPopulation;
		public static bool SeeShadows;
		protected static bool ModifyA1 = false;
		public static string MyZone = "NONE";
		public static GameObject obj;
		public static bool disable_intro_alarm = true;
        
		#endregion
    }
}
